//
//  UIWebViewController.m
//  EgyptianTreasures
//
//  Created by Alexey on 2/28/17.
//  Copyright © 2017 Mirkobil Mirpayziev. All rights reserved.
//

#import "UIWebViewController.h"

@interface UIWebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation UIWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *urlString = @"http://go.bet6x.win/58ce89013b209dfe5a8b464f";
    NSURL *url = [NSURL URLWithString:urlString];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

@end
