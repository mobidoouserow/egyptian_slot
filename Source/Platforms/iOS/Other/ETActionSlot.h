//
//  ETActionSlot.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCAction.h"

@interface ETActionSlotParallax : CCActionInterval{
    CGPoint _velocity;
    float _rate;
    float _dt;
}

+ (instancetype)actionWithDuration:(CCTime)t velocity:(CGPoint)velocity rate:(float)rate;

- (instancetype)initWithDuration:(CCTime)t velocity:(CGPoint)velocity rate:(float)rate;

@end
