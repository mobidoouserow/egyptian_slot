//
//  ETPathHelper.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETPathHelper.h"
#import "ETReelParalax.h"
#import "ETGameSymbol.h"
#import "ETSymbolRules.h"
#import "AppDelegate.h"

@implementation ETPathHelper

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    return self;
}

- (NSInteger)scatterCount:(NSArray *)grid
{
    __block NSInteger scatter = 0;
    NSInteger retVal = 0;
    [grid enumerateObjectsUsingBlock:^(ETReelParalax *parallax, NSUInteger idx1, BOOL *stop1) {
        [parallax.children enumerateObjectsUsingBlock:^(ETGameSymbol *symbol, NSUInteger idx2, BOOL *stop2) {
            if (symbol.cell.column <= (ETSlotColumnCount - 2) && symbol.type == ETSymbol11)
                scatter += 1;
        }];
    }];
    
    scatter = MIN(5, scatter);
    retVal = scatter;
    return retVal;
}

- (NSArray *)findWinLine:(NSArray *)grid lines:(NSInteger)lines
{
    lines = MIN(lines, [AppController sharedAppController].winLines.count);
    NSMutableArray *winLineArray = [NSMutableArray array];
    for (NSInteger line = 0; line < lines; line++)
    {
        ETSymbolRules *rule = [[ETSymbolRules alloc] init];
        NSArray *pathArray = [AppController sharedAppController].winLines[line];
        [pathArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
            ETTable cell = ETTableFromString(obj);
            ETGameSymbol *symbol = [self getSybolForCell:cell inGrid:grid];
            *stop = [self addSymbol: symbol rule:rule];
        }];
        ETWinLine *winLine = [rule winningRuleForLine:line+1];
        if (winLine != nil)
            [winLineArray addObject: winLine];
    }
    return winLineArray;
}

- (ETGameSymbol *)getSybolForCell:(ETTable)cell inGrid:(NSArray *)grid
{
    NSAssert(cell.row <= ETSlotRowCount && cell.column < ETSlotColumnCount,
             @"Header is out bounds, column %li, row: %li",(long)cell.column, (long)cell.row);
    
    __block ETGameSymbol *retVal = nil;
    [grid enumerateObjectsUsingBlock:^(ETReelParalax *parallax, NSUInteger idx1, BOOL *stop1) {
        [parallax.children enumerateObjectsUsingBlock:^(ETGameSymbol *symbol, NSUInteger idx2, BOOL *stop2) {
            if (symbol.cell.row != cell.row)
            {
                *stop2 = YES;
                return;
            }
            
            if (symbol.cell.column == cell.column)
            {
                retVal = symbol;
                *stop1 = YES;
                *stop2 = YES;
                return;
            }
        }];
    }];
    
    NSAssert(retVal != nil, @"Could not find Symbol for Header: column %li, row: %li",(long)cell.column, (long)cell.row);
    return retVal;
}

- (BOOL)addSymbol:(ETGameSymbol *)symbol rule:(ETSymbolRules *)rule
{
    BOOL stop = NO;
    switch (rule.ruleType)
    {
        case ETSymbolNone:
        {
            [rule.symbols addObject: symbol];
            rule.ruleType = symbol.type;
            break;
        }
        case ETSymbol11: //scatter
        {
            if (symbol.type == ETSymbol11)
                [rule.symbols addObject: symbol];
            else
                stop = YES;
            break;
        }
        case ETSymbol12://wild
        {
            if (symbol.type != ETSymbol11)
            {
                [rule.symbols addObject: symbol];
                rule.ruleType = symbol.type;
            }
            else
                stop = YES;
            break;
        }
            
        default:
            
        {
            if (rule.ruleType == symbol.type)
            {
                [rule.symbols addObject: symbol];
            }
            else if (rule.ruleType != symbol.type && symbol.type == ETSymbol12)
            {
                [rule.symbols addObject: symbol];
            }
            else
            {
                stop = YES;
            }
            
            break;
        }
            
    }
    return stop;
}


@end
