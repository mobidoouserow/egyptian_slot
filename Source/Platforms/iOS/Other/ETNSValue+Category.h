//
//  ETNSValue+Category.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ETTableSwap( a, b ) ({ __typeof__(a) temp  = (a);a = b; b = temp; })

typedef NS_ENUM(NSInteger, ETTableType){
    ETTableTypeColumn,
    ETTableTypeRow,
};

struct ETTable {
    NSInteger row;
    NSInteger column;
};
typedef struct ETTable ETTable;

struct ETTableCollection {
    ETTable a;
    ETTable b;
};
typedef struct ETTableCollection ETTableCollection;

static inline ETTable ETTableMake( NSInteger column, NSInteger row )
{
    ETTable h; h.column = column; h.row = row; return h;
}

static inline ETTableCollection ETTableCollectionMake( ETTable a, ETTable b )
{
    ETTableCollection h; h.a = a; h.b = b; return h;
}

static inline BOOL ETTableEqualToTable( ETTable a, ETTable b )
{
    return  a.column == b.column && a.row == b.row;
}

static inline BOOL ETTableCollectionEqualToCollection(ETTableCollection c1, ETTableCollection c2, BOOL sensitive)
{
    if (sensitive)
    {
        return ETTableEqualToTable(c1.a, c2.a) && ETTableEqualToTable(c1.b, c2.b);
    }
    else
    {
        return (ETTableEqualToTable(c1.a, c2.a) && ETTableEqualToTable(c1.b, c2.b)) ||
        (ETTableEqualToTable(c1.a, c2.b) && ETTableEqualToTable(c1.b, c2.a));
    }
}

static inline ETTable ccpToTable(CGPoint p)
{
    return ETTableMake(p.x, p.y);
}

static inline CGPoint ccpFromTable(ETTable t)
{
    return CGPointMake(t.column, t.row);
}

static inline NSString *NSStringFromETTable(ETTable table)
{
    return NSStringFromCGPoint(ccpFromTable(table));
}

static inline ETTable ETTableFromString(NSString *string)
{
    return ccpToTable(CGPointFromString(string));
}

static inline ETTable ETTableClamp( ETTable v, ETTable min, ETTable max )
{
    return ETTableMake(MIN(MAX(v.column, min.column), max.column), MIN(MAX(v.row, min.row), max.row));
}

@interface NSValue(ETTable)

- (ETTable)ETTableValue;

+ (NSValue *)valueWithETTable:(ETTable)table;

- (ETTableCollection)ETTableCollectionValue;

+ (NSValue *)valueWithETTableCollection:(ETTableCollection)tableCollection;

@end

