//
//  ETNumbers.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETNumbers.h"
#import "CCControlSubclass.h"

#define kCCFatFingerExpansions 70

@implementation ETNumbers

- (void)setNumberEnable:(BOOL)numberEnable
{
    if (_numberEnable != numberEnable)
        _numberEnable = numberEnable;
    [self stateChanged];
}

- (void)updatePropertiesForState:(CCControlState)state
{
    CCSpriteFrame* spriteFrame = [self backgroundSpriteFrameForState:state];
    if (!spriteFrame) spriteFrame = [self backgroundSpriteFrameForState:CCControlStateNormal];
    self.background.spriteFrame = spriteFrame;
    
    [self needsLayout];
}

- (void) stateChanged
{
    if (self.numberEnable)
        [self updatePropertiesForState:CCControlStateNormal];
    else
        [self updatePropertiesForState:CCControlStateDisabled];
}

@end
