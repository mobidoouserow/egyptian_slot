//
//  ETReelParalax.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETReelParalax.h"
#import "ETGameSymbol.h"

@implementation ETReelParalax

- (void)updateParallax:(CCTime)dt velocity:(CGPoint)velocity
{
    self.position = ccpAdd(self.position, ccpMult(velocity, dt));
    
    for (CGPointObject *point in _parallaxArray)
    {
        ETGameSymbol *symbol = (ETGameSymbol *)point.child;
        CGPoint position = [self convertToWorldSpace:symbol.position];
        if ( position.y < symbol.tileSize.height * 0.25)
        {
            CGPoint viewSize = ccp(0.0, symbol.tileSize.height * 4 );
            if (symbol.replaced == NO)
            {
                point.offset = ccpAdd(point.offset, viewSize);
                [symbol changeType];
                [symbol updateCellWithOffset:point.offset];
            }
            symbol.replaced = YES;
            break;
        }
        else
            symbol.replaced = NO;
    }
}

- (void)testDistance:(NSString *)text
{
    NSString *string = @"";
    for (NSInteger i = 1; i < [_parallaxArray count]; i++)
    {
        CGPointObject *point1 = _parallaxArray[i-1];
        CGPointObject *point2 = _parallaxArray[i];
        string = [string stringByAppendingString:[NSString stringWithFormat:@"%f, ",ccpDistance(point1.offset, point2.offset)]];
    }
    NSLog(@"%@", [NSString stringWithFormat:@"%@: %@", text, string]);
}

- (void)deltaOffset
{
    __block float max = 10000.0f;
    __block ETGameSymbol *closestSymbol = nil;
    __weak typeof(self)weakSelf = self;
    [self.children enumerateObjectsUsingBlock:^(ETGameSymbol *symbol, NSUInteger idx, BOOL *stop) {
        float distance = ccpDistance([symbol calculatePositionForCell:ETTableMake(0, symbol.cell.row)],
                                     ccpAdd(weakSelf.position, symbol.position));
        if (distance < max)
        {
            max = distance;
            closestSymbol = symbol;
        }
    }];
    
    NSAssert(closestSymbol != nil, @"Symbol should not be nil");
    [self.children enumerateObjectsUsingBlock:^(ETGameSymbol *symbol, NSUInteger idx, BOOL *stop) {
        if (symbol == closestSymbol)
            closestSymbol.color = [CCColor blueColor];
        else
            symbol.color = [CCColor whiteColor];
    }];
}

- (CGPoint)calculatePosition
{
    __block CGPoint delta = CGPointZero;
    __weak typeof(self)weakSelf = self;
    [self.children enumerateObjectsUsingBlock:^(ETGameSymbol *obj, NSUInteger idx, BOOL *stop) {
        ETTable cell = ETTableMake(0, obj.cell.row);
        if (ETTableEqualToTable(obj.cell, cell))
        {
            delta = ccpSub([obj calculatePositionForCell:cell], ccpAdd(weakSelf.position,obj.position));
        }
    }];
    
    NSAssert(CGPointEqualToPoint(delta, CGPointZero) == NO, @"Zero Delta");
    return ccpAdd(self.position, delta);
}


@end
