//
//  ETEmailManager.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETEmailManager.h"
#import "Reachability.h"

@implementation ETEmailManager


+ (instancetype)emailWithRecipient:(NSArray *)recipient title:(NSString *)title body:(NSString *)body
{
    return [[self alloc] initWithRecipient:recipient title:title body:body];
}

- (instancetype)initWithRecipient:(NSArray *)recipient title:(NSString *)title body:(NSString *)body
{
    self = [super init];
    if (self)
    {
        self.title = title;
        self.body = body;
        self.recipient = recipient;
    }
    return self;
}

- (void)show
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        if ([_delegate respondsToSelector:@selector(networkNotReachable)])
            [_delegate networkNotReachable];
        
        [self alertWithTitle:@"Warning" message:@"No internet connection. Please try again later!"];
    }
    else
    {
        _picker = [self createMailComposeViewControllerWithRecipients:_recipient subject:_title body:_body];
        
        if ([_delegate respondsToSelector:@selector(mailComposeControllerWillPresent)])
            [_delegate mailComposeControllerWillPresent];
        
        [[CCDirector sharedDirector] pause];
        [[CCDirector sharedDirector] presentViewController:_picker animated:YES completion:NULL];
    }
}

- (void)showWithAttachment:(NSData *)data mimeType:(NSString *)mimeType fileName:(NSString *)fileName
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        if ([_delegate respondsToSelector:@selector(networkNotReachable)])
            [_delegate networkNotReachable];
        
        [self alertWithTitle:@"Warning" message:@"No internet connection. Please try again later!"];
    }
    else
    {
        _picker = [self createMailComposeViewControllerWithRecipients:_recipient subject:_title body:_body];
        [_picker addAttachmentData:data mimeType:mimeType fileName:fileName];
        
        if ([_delegate respondsToSelector:@selector(mailComposeControllerWillPresent)])
            [_delegate mailComposeControllerWillPresent];
        
        [[CCDirector sharedDirector] pause];
        [[CCDirector sharedDirector] presentViewController:_picker animated:YES completion:NULL];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [[CCDirector sharedDirector] resume];
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    if ([_delegate respondsToSelector:@selector(mailComposeControllerDidFinishWithResult:)])
        [_delegate mailComposeControllerDidFinishWithResult:result];
}

#pragma mark Helper Method

- (MFMailComposeViewController *)createMailComposeViewControllerWithRecipients:(NSArray *)recipient subject:(NSString *)subject body:(NSString *)body
{
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    mail.modalPresentationStyle = UIModalPresentationFullScreen;
    mail.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    mail.mailComposeDelegate = self;
    
    [mail setToRecipients: recipient];
    [mail setSubject: subject];
    [mail setMessageBody:body isHTML:NO];
    
    return mail;
}

-(void)alertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert =   [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alert addAction:ok];
    
    [[CCDirector sharedDirector] presentViewController:alert animated:YES completion:nil];
}


@end
