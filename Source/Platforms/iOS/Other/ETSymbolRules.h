//
//  ETSymbolRules.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@class ETWinLine;
@interface ETSymbolRules : CCNode

@property (nonatomic, strong)NSMutableArray *symbols;

@property (nonatomic, assign)ETSymbolType ruleType;

- (ETWinLine *)winningRuleForLine:(NSInteger)line;

@end

@protocol ETWinLineDelegate <NSObject>
- (void)setLine:(NSInteger)line visible:(BOOL)visible;
@end

@interface ETWinLine : CCNode

@property (nonatomic, weak)id <ETWinLineDelegate> delegate;

@property (nonatomic, assign)NSInteger line;

@property (nonatomic, assign)NSInteger multiplier;

@property (nonatomic, strong)NSArray *symbols;

- (instancetype)initWithLine:(NSInteger)line multiplier:(NSInteger)multiplier symbols:(NSArray *)symbols;

- (void)show;

- (void)hide;


@end