//
//  ETStoreManager.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol ETStoreDelegate <NSObject>

@required

- (void)transactionSuccessfulForIdentifier:(NSString *)identifier;

@optional

- (void)transactionFailedForIdentifier:(NSString *)identifier;
- (void)didReceiveResponseForProduct:(SKProduct *)product;

@end


@interface ETStoreManager : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate>

@property (nonatomic, weak)id <ETStoreDelegate> delegate;

+ (instancetype)sharedStoreManager;

- (void)startWithProducts:(NSSet *)products delegate:(id <ETStoreDelegate>)delegate;

- (SKProduct *)productForIdentifier:(NSString *)identifier;

- (NSString *)localPriceForProduct:(NSString *)identifier;

- (void)purchase:(NSString *)identifier;

- (void)restore;



@end
