//
//  ETReelParalax.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCParallaxNode.h"

@interface ETReelParalax : CCParallaxNode

@property (nonatomic ,assign)CGSize tileSize;

- (void)updateParallax:(CCTime)dt velocity:(CGPoint)velocity;

- (void)deltaOffset;

- (CGPoint)calculatePosition;

@end
