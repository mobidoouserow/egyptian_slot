//
//  ETPercentSettings.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETPercentSettings.h"
#import "ETSlider.h"
#import "ETButton.h"
#import "ETEmailManager.h"
#import "AppDelegate.h"

#define K_COLOR_NODE @"COLOR"

@interface ETPercentSettings ()
@property (nonatomic, strong)CCLayoutBox *sliderLayout;
@property (nonatomic, strong)ETEmailManager *email;
@end

@implementation ETPercentSettings

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;

    _contentSize = [CCDirector sharedDirector].viewSize;
    self.userInteractionEnabled = YES;
    
    [self addChild:[self nodeColor]];

    [self updateValue];
    
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (CCNodeColor *)nodeColor
{
    CCNodeColor *node = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75]];
    node.position = ccp(0.0, _contentSize.height);
    node.name = K_COLOR_NODE;
    
    _sliderLayout = [self symbolsSliders];
    [node addChild:_sliderLayout];
    [node addChild:[self buttons]];
    
    [node runAction:[CCActionEaseBounceOut actionWithAction:[CCActionMoveTo actionWithDuration:0.6 position:CGPointZero]]];
    
    return node;
}

- (CCLayoutBox *)symbolsSliders
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    for (ETSymbolType i = ETSymbol1; i <= ETSymbol12; i++)
    {
        [layout addChild:[self createSliderForType:i]];
    }
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.55);
    layout.spacing = ETUniversalValue(40);
    layout.rotation = 90;

    return layout;
}

- (ETSlider *)createSliderForType:(ETSymbolType)symbol
{
    NSString *imageName = [NSString stringWithFormat:@"symbol%li_00.png",(long)symbol];
    CCSpriteFrame *handle = [CCSpriteFrame frameWithImageNamed:imageName];
    ETSlider *slider = [[ETSlider alloc] initWithBackground:[self sliderBackground] andHandleImage:handle max:100];
    slider.preferredSize = CGSizeMake([CCDirector sharedDirector].viewSize.height * 0.65, slider.preferredSize.height);
    slider.handle.scale = 0.4;
    slider.handle.rotation = -90;
    slider.floatValue = YES;
    slider.position = ccp(_contentSize.width * 0.5, _contentSize.height * 0.5);
    slider.anchorPoint = ccp(0.5, 0.5);
    slider.name = [NSString stringWithFormat:@"%li",(long)symbol];
    slider.endStop = slider.handle.contentSize.height * 0.5 * slider.handle.scale;
    slider.handle.hitAreaExpansion = slider.handle.contentSize.height * 0.3;
    [slider setTarget:self selector:@selector(onSlider:)];
    
    return slider;
}

- (CCLayoutBox *)buttons
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self createButtonWithTitle:@"send" selector:@selector(onSend:)]];
    [layout addChild:[self createButtonWithTitle:@"print" selector:@selector(onPrint:)]];
    [layout addChild:[self createButtonWithTitle:@"reset" selector:@selector(onReset:)]];
    [layout addChild:[self createButtonWithTitle:@"close" selector:@selector(onClose:)]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.7, 0.15);
    layout.spacing = ETUniversalValue(6);
    
    return layout;
}

- (ETButton *)createButtonWithTitle:(NSString *)title selector:(SEL)selector
{
    ETButton *button = [ETButton buttonWithTitle:title spriteFrame:[self buttonSpriteFrame]];
    button.label.fontName = ETFontName;
    button.label.anchorPoint = ccp(0.5, 0.4);
    button.label.fontSize = ETUniversalValue(10);
    [button setTarget:self selector:selector];
    return button;
}

- (CCSpriteFrame *)buttonSpriteFrame
{
    CGSize size = CGSizeMake(ETUniversalValue(60), ETUniversalValue(25));
    CCDrawNode *draw = [self drawButton:size];
    CCRenderTexture *render = [CCRenderTexture renderTextureWithWidth:size.width height:size.height];
    
    [render begin];
    [draw visit];
    [render end];
    
    return [render.texture createSpriteFrame];
}


- (CCSpriteFrame *)sliderBackground
{
    CGSize size = CGSizeMake(ETUniversalValue(50), ETUniversalValue(2));
    CCDrawNode *draw = [self drawLine:size];
    CCRenderTexture *render = [CCRenderTexture renderTextureWithWidth:size.width height:size.height];

    [render begin];
    [draw visit];
    [render end];
    
    return [render.texture createSpriteFrame];
}

- (void)updateValue
{
    __weak typeof(_sliderLayout)weakLayout = _sliderLayout;
    [_sliderLayout.children enumerateObjectsUsingBlock:^(ETSlider *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[ETSlider class]])
        {
            obj.min = 0.01;
            obj.max = 100.0 - 5 * [weakLayout.children count];
            NSNumber *number = [AppController sharedAppController].symbolPercent[[obj.name integerValue] - 1];
            obj.value = [number floatValue];
        }
    }];

}

- (CCDrawNode *)drawButton:(CGSize)size
{
    CCDrawNode *draw = [[CCDrawNode alloc] init];

    NSInteger count = 4;
    CGPoint *points = malloc(sizeof(CGPoint) * count);
    
    points[0] = ccp(-size.width, -size.height);
    points[1] = ccp( size.width, -size.height);
    points[2] = ccp( size.width,  size.height);
    points[3] = ccp(-size.width,  size.height);
    
    [draw drawPolyWithVerts:points count:count fillColor:[CCColor grayColor] borderWidth:1.0 borderColor:[CCColor whiteColor]];

    free(points);
    
    return draw;
}

- (CCDrawNode *)drawLine:(CGSize)size
{
    
    CCDrawNode *draw = [[CCDrawNode alloc] init];
    CCColor *color = [CCColor colorWithRed:0.965 green:0.714 blue:0.310 alpha:1];
    [draw drawSegmentFrom:ccp(size.height, size.height * 0.5)
                       to:ccp(size.width - size.height, size.height * 0.5)
                   radius:size.height
                    color:color];
    [draw drawDot:ccp(0, size.height * 0.5)
           radius:size.height / 2
            color:color];
    [draw drawDot:ccp(size.width, size.height * 0.5)
           radius:size.height / 2
            color:color];

    return draw;
}

- (void)onSlider:(ETSlider *)sender
{
    __block double total = 0;
    [sender.parent.children enumerateObjectsUsingBlock:^(ETSlider *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[ETSlider class]])
            total += obj.value;
    }];
    
    double delta = round(total - 100.0f);
    [sender.parent.children enumerateObjectsUsingBlock:^(ETSlider *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[ETSlider class]] && obj != sender)
        {
            obj.value += -1.0f * (delta * (obj.value / 100.0f));
            [AppController sharedAppController].symbolPercent[[obj.name integerValue] - 1] = @(obj.value);
        }
    }];
    [AppController sharedAppController].symbolPercent[[sender.name integerValue] - 1] = @(sender.value);
    
}

- (NSString *)log
{
    __block NSString *string = @"";
    [_sliderLayout.children enumerateObjectsUsingBlock:^(ETSlider *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        string = [string stringByAppendingString:[NSString stringWithFormat:@"@%.2f, ", obj.value]];
    }];
    return string;
}

- (void)onPrint:(id)sender
{
    [[AppController sharedAppController] click];
    NSLog(@"\n");
    NSLog(@"--------------------------------------------\n");
    NSLog(@"*********************************************\n");
    NSLog(@"//To save currect settings, please replace line #58 in AppDeleage.m class with this code\n");
    NSLog(@"_symbolPercent = [NSMutableArray arrayWithObjects:%@ nil];\n", [self log]);
    NSLog(@"*********************************************\n");
    NSLog(@"--------------------------------------------\n");
    NSLog(@"\n");
}

- (void)onReset:(id)sender
{
    [[AppController sharedAppController] click];
    __weak typeof(_sliderLayout)weakLayout = _sliderLayout;
    [_sliderLayout.children enumerateObjectsUsingBlock:^(ETSlider *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[ETSlider class]])
        {
            obj.min = 0.01;
            obj.max = 100.0 - 5/*?*/ * [weakLayout.children count];
            obj.value = 100.0 / [weakLayout.children count];
            [AppController sharedAppController].symbolPercent[[obj.name integerValue] - 1] = @(obj.value);
        }
    }];
}

- (void)clean
{
    [self removeFromParentAndCleanup:YES];
}

- (void)onClose:(id)sender
{
    [[AppController sharedAppController] click];
    CCNode *node = [self getChildByName:K_COLOR_NODE recursively:NO];
    [node runAction:[CCActionSequence actionOne:[CCActionEaseInOut actionWithAction:
                                                 [CCActionMoveTo actionWithDuration:0.3 position:ccp(0.0, _contentSize.height)] rate:2.0]
                                            two:[CCActionCallFunc actionWithTarget:self selector:@selector(clean)]]];
}


- (void)onSend:(id)sender
{
    [[AppController sharedAppController] click];
    NSString *displayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *title = [NSString stringWithFormat:@"Symbol Percent for %@ app", displayName];
    NSString *code = [NSString stringWithFormat:@"_symbolPercent = [NSMutableArray arrayWithObjects:%@ nil];\n", [self log]];
    NSString *body = [NSString stringWithFormat:@"//To save currect settings, please replace line #58 in AppDeleage.m class with this code\n %@", code];
    
    _email = nil;
    _email = [[ETEmailManager alloc] initWithRecipient:@[@""] title:title body:body];
    [_email show];
}

@end
