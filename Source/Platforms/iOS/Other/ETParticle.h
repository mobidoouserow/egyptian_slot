//
//  ETParticle.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/16/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCParticleSystem.h"

@interface ETParticleCoin : CCParticleSystem

@end

@interface ETParticleStar : CCParticleSystem

@end
