//
//  ETParticle.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/16/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETParticle.h"

@implementation ETParticleCoin

-(id) init
{
    return [self initWithTotalParticles:1000];
}

-(id) initWithTotalParticles:(NSUInteger)p
{
    if( (self=[super initWithTotalParticles:p]) ) {
        
        CCTexture *textuer = [CCTexture textureWithFile:@"market_coin.png"];
        
        // _duration
        _duration = 2.0f;
        
        self.emitterMode = CCParticleSystemModeGravity;
        
        // Gravity Mode: gravity
        self.gravity = ccp(10,-10);
        
        // Gravity Mode: radial
        self.radialAccel = 0;
        self.radialAccelVar = 0;
        
        // Gravity Mode: tagential
        self.tangentialAccel = 0;
        self.tangentialAccelVar = 0;
        
        // Gravity Mode: speed of particles
        self.speed = 350;
        self.speedVar = 50;
        
        // _angle
        _angle = -90;
        _angleVar = 0;
        
        
        // emitter position
        self.position = (CGPoint) {
            [[CCDirector sharedDirector] viewSize].width / 2,
            [[CCDirector sharedDirector] viewSize].height + textuer.contentSize.width
        };
        self.posVar = ccp( [[CCDirector sharedDirector] viewSize].width / 2, 0 );
        
        // _life of particles
        _life = _duration;
        _lifeVar = 0.1;
        
        _startSize = textuer.contentSize.width;
        _startSizeVar = textuer.contentSize.width / 4;
        _endSize = CCParticleSystemStartSizeEqualToEndSize;
        
        // emits per second
        _emissionRate = _totalParticles/_life;
        
        // color of particles
        _startColor = ccc4f(1.0f, 1.0f, 1.0f, 1.0f);
        _startColorVar = ccc4f(0.0f, 0.0f, 0.0f, 0.0f);
        _endColor = ccc4f(1.0f, 1.0f, 1.0f, 1.0f);
        _endColorVar = ccc4f(0.0f, 0.0f, 0.0f, 0.0f);
        
        //
        self.texture = textuer;
        self.blendAdditive = NO;
        self.autoRemoveOnFinish = YES;
    }
    
    return self;
}

@end

@implementation ETParticleStar

-(id) init
{
    return [self initWithTotalParticles:1000];
}

-(id) initWithTotalParticles:(NSUInteger)p
{
    if( (self=[super initWithTotalParticles:p]) ) {
        
        CCTexture *textuer = [CCTexture textureWithFile:@"particle_star.png"];
        
        // _duration
        _duration = 2.0f;
        
        self.emitterMode = CCParticleSystemModeGravity;
        
        // Gravity Mode: gravity
        self.gravity = ccp(10,-10);
        
        // Gravity Mode: radial
        self.radialAccel = 0;
        self.radialAccelVar = 0;
        
        // Gravity Mode: tagential
        self.tangentialAccel = 0;
        self.tangentialAccelVar = 0;
        
        // Gravity Mode: speed of particles
        self.speed = 600;
        self.speedVar = 100;
        
        // _angle
        _angle = -90;
        _angleVar = 0;
        
        
        // emitter position
        self.position = (CGPoint) {
            [[CCDirector sharedDirector] viewSize].width / 2,
            [[CCDirector sharedDirector] viewSize].height + textuer.contentSize.width
        };
        self.posVar = ccp( [[CCDirector sharedDirector] viewSize].width / 2, 0 );
        
        // _life of particles
        _life = _duration;
        _lifeVar = 0.1;
        
        _startSize = textuer.contentSize.width / 2;
        _startSizeVar = textuer.contentSize.width / 2;
        _endSize = CCParticleSystemStartSizeEqualToEndSize;
        
        // emits per second
        _emissionRate = _totalParticles/_life;
        
        // color of particles
        _startColor = ccc4f(1.0f, 1.0f, 1.0f, 1.0f);
        _startColorVar = ccc4f(0.0f, 0.0f, 0.0f, 0.0f);
        _endColor = ccc4f(1.0f, 1.0f, 1.0f, 1.0f);
        _endColorVar = ccc4f(0.0f, 0.0f, 0.0f, 0.0f);
        
        //
        self.texture = textuer;
        self.blendAdditive = NO;
        self.autoRemoveOnFinish = YES;
    }
    
    return self;
}

@end
