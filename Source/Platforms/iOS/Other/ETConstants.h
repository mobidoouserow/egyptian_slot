//
//  ETConstants.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/5/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#ifndef ETConstants_h
#define ETConstants_h

@protocol ETEgyptianTreasuresDelegate <NSObject>

- (void)start;
- (void)spin;
- (void)shop;
- (void)rate;
- (void)info;
- (void)percent;

- (void)particle:(NSInteger)count;
- (void)line:(NSInteger)line;
- (void)bonus:(NSInteger)count;

@end

typedef NS_ENUM(NSUInteger, ETSymbolType){
    ETSymbolNone,
    ETSymbol1,
    ETSymbol2,
    ETSymbol3,
    ETSymbol4,
    ETSymbol5,
    ETSymbol6,
    ETSymbol7,
    ETSymbol8,
    ETSymbol9,
    ETSymbol10,
    ETSymbol11, // scatter
    ETSymbol12  // wild
};

typedef NS_ENUM(NSUInteger, ETStoneSymbolType){
    ETStoneSymbolNode,
    ETStoneSymbol6 = 6,
    ETStoneSymbol7,
    ETStoneSymbol8,
    ETStoneSymbol9,
    ETStoneSymbol10,
    ETStoneSymbolJ,
    ETStoneSymbolQ,
    ETStoneSymbolK,
    ETStoneSymbolT,
};


static inline CGFloat ETUniversalValue(CGFloat value)
{
    return value * ([UIScreen mainScreen].bounds.size.width / 568.0f);
}

#define ETSlotColumnCount    4
#define ETSlotRowCount       5

#define K_ANIMATION_FRAMES_IN_SEC 25

typedef NS_ENUM(NSUInteger, ETDevice) {
    ETDeviceNone,
    ETDeviceiPhone5,
    ETDeviceiPhone4,
    ETDeviceiPhone6,
    ETDeviceiPhone6Plus,
    ETDeviceiPad
};

static inline double ETFloatRandom(double min, double max)
{
    return ((double)arc4random() / (double)0x100000000) * (max - min) + min;
}


#endif /* ETConstants_h */
