//
//  ETSlider.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCSlider.h"

@interface ETSlider : CCSlider

@property (nonatomic, assign)float beganDrag;

@property (nonatomic, assign)NSInteger tag;

@property (nonatomic, assign)double preValue;

@property (nonatomic, assign)double value;

@property (nonatomic, readonly)CCLabelTTF *label;

@property (nonatomic, assign)double min;

@property (nonatomic, assign)double max;

@property (nonatomic, assign)BOOL floatValue;

@property (nonatomic, copy)bool (^touchBlock)(id, CCTouch *);

- (instancetype)initWithMax:(double)max min:(double)min value:(double)value;

- (instancetype)initWithBackground:(CCSpriteFrame *)background andHandleImage:(CCSpriteFrame *)handle max:(double)max;


@end
