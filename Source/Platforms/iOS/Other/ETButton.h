//
//  ETButton.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCButton.h"

@interface ETButton : CCButton

@property (nonatomic, assign)NSInteger number;

@property (nonatomic, assign)BOOL freeLabelPosition;

@property (nonatomic, assign)CGPoint originalLabelPosition;

@end
