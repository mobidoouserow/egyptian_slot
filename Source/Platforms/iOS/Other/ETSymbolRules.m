//
//  ETSymbolRules.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETSymbolRules.h"
#import "ETGameSymbol.h"

@implementation ETSymbolRules

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    _ruleType = ETSymbolNone;
    _symbols = [[NSMutableArray alloc] init];
    
    return self;
}

- (ETWinLine *)winningRuleForLine:(NSInteger)line
{
    if ([_symbols count] <= 1)
        return nil;
    
    ETWinLine *winLine = nil;
    NSInteger multiplier = [self ruleForType:_ruleType symbols:_symbols];
    
    if (multiplier != 0)
        winLine = [[ETWinLine alloc]initWithLine:line multiplier:multiplier symbols:_symbols];
        
        return winLine;
}

- (NSInteger)ruleForType:(ETSymbolType)symbolType symbols:(NSArray *)symbols
{
    NSInteger value = 0;
    
    switch (symbolType)
    {
        case ETSymbol1:
        case ETSymbol2:
        {
            switch ([symbols count])
            {
                case 3: value =   5; break;
                case 4: value =  20; break;
                case 5: value = 100; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol3:
        case ETSymbol4:
        {
            switch ([symbols count])
            {
                case 3: value =   7; break;
                case 4: value =  25; break;
                case 5: value = 150; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol5:
        {
            switch ([symbols count])
            {
                case 3: value =  10; break;
                case 4: value =  30; break;
                case 5: value = 200; break;
                default: break;
            }
            break;
        }
        case ETSymbol6:
        case ETSymbol7:
        
        {
            switch ([symbols count])
            {
                case 3: value =  15; break;
                case 4: value =  75; break;
                case 5: value = 500; break;
                default: break;
            }
            break;
        }
        case ETSymbol8:
        {
            switch ([symbols count])
            {
                case 2: value =    5; break;
                case 3: value =  100; break;
                case 4: value =  250; break;
                case 5: value = 2500; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol9:
        {
            switch ([symbols count])
            {
                case 2: value =    5; break;
                case 3: value =   50; break;
                case 4: value =  100; break;
                case 5: value = 1000; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol10:
        {
            switch ([symbols count])
            {
                case 2: value =   10; break;
                case 3: value =  150; break;
                case 4: value =  500; break;
                case 5: value = 5000; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol11:// scatter
        {
            switch ([symbols count])
            {
                case 3: value =   3; break;
                case 4: value =  10; break;
                case 5: value = 100; break;
                default: break;
            }
            break;
        }
            
        case ETSymbol12:// wild
        {
            switch ([symbols count])
            {
                case 2: value =   15; break;
                case 3: value =  200; break;
                case 4: value = 1000; break;
                case 5: value = 5000; break;
                default: break;
            }
            break;
        }
        default: NSAssert(NO, @"Unknown symbol type"); break;
    }
    return value;
}

@end

@implementation ETWinLine

- (instancetype)initWithLine:(NSInteger)line multiplier:(NSInteger)multiplier symbols:(NSArray *)symbols
{
    self = [super init];
    if (!self) return(nil);
    
    _line = line;
    _multiplier = multiplier;
    _symbols = symbols;
    
    return self;
}

- (void)show
{
    if ([_delegate respondsToSelector:@selector(setLine:visible:)])
        [_delegate setLine:_line visible:YES];
    
    [_symbols enumerateObjectsUsingBlock:^(ETGameSymbol *obj, NSUInteger idx, BOOL *stop) {
        obj.symbolFrameContour = _line;
    }];
}

- (void)hide
{
    if ([_delegate respondsToSelector:@selector(setLine:visible:)])
        [_delegate setLine:_line visible:NO];
    
    [_symbols enumerateObjectsUsingBlock:^(ETGameSymbol *obj, NSUInteger idx, BOOL *stop) {
        [obj hideSymbolFrame];
    }];
    
}

@end

