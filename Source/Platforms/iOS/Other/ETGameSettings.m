//
//  ETGameSettings.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameSettings.h"
#import "GameCenterManager.h"

static NSString *const ETGSFileName = @"settings.plist";

static NSString *const ETGSLines = @"lines";
static NSString *const ETGSCredits = @"credits";
static NSString *const ETGSPlayTime = @"playTime";
static NSString *const ETGSRated = @"rated";
static NSString *const ETGSSound = @"sound";
static NSString *const ETGSAds = @"ads";
static NSString *const ETGSLineBet = @"linebet";
static NSString *const ETGSHighScore = @"highscore";

@implementation ETGameSettings

+ (instancetype)defaultSettings
{
    static ETGameSettings *singleton;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singleton = [[ETGameSettings alloc] init];
    });
    
    return singleton;
}

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    _setting = [self settingData];

    _lines      = [_setting[ETGSLines] integerValue];
    _credits    = [_setting[ETGSCredits] floatValue];
    _playTime   = [_setting[ETGSPlayTime] integerValue];
    _rated      = [_setting[ETGSRated] boolValue];
    _sound      = [_setting[ETGSSound] boolValue];
    _ads        = [_setting[ETGSAds] boolValue];
    _lineBet    = [_setting[ETGSLineBet] integerValue];
    _highScore  = [_setting[ETGSHighScore] floatValue];

    return self;
}

+ (void)removeSettingFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [ paths[0] stringByAppendingPathComponent: ETGSFileName ];
    
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:path])
        [manager removeItemAtPath:path error:nil];
}

- (NSMutableDictionary *)settingData
{
    return [NSMutableDictionary dictionaryWithContentsOfFile:[self coreData]];
}

- (NSString *)coreData
{
    NSString *path = [self getFilePath];
    
    if (![ [NSFileManager defaultManager] fileExistsAtPath:path] )
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [dictionary setValue:@20                        forKey: ETGSLines];
        [dictionary setValue:@(K_DEFAULT_CREDIT_BALANCE)forKey: ETGSCredits];
        [dictionary setValue:@0                         forKey: ETGSPlayTime];
        [dictionary setValue:@NO                        forKey: ETGSRated];
        [dictionary setValue:@YES                       forKey: ETGSSound];
        [dictionary setValue:@YES                       forKey: ETGSAds];
        [dictionary setValue:@0                         forKey: ETGSLineBet];
        [dictionary setValue:@0.0                       forKey: ETGSHighScore];
    
        [dictionary writeToFile:path atomically:YES];
    }
    
    return path;
}

- (NSString *)getFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [ paths[0] stringByAppendingPathComponent: ETGSFileName ];
}

- (void)saveSettings:(NSDictionary *)settings
{
    [ settings writeToFile:[self coreData] atomically:YES ];
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [_setting setValue:value forKey:key];
    [self saveSettings:_setting];
}

- (void)setLines:(NSInteger)lines
{
    if (_lines == lines)
        return;
    _lines = lines;
    [self setValue:@(lines) forKey:ETGSLines];
}

- (void)setCredits:(CGFloat)credits
{
    if (_credits == credits)
        return;
    _credits = credits;
    self.highScore = credits;
    [self setValue:@(credits) forKey:ETGSCredits];
}

- (void)setHighScore:(CGFloat)highScore
{
    if (highScore <= _highScore)
        return;
    _highScore = highScore;
    
#if K_ENABLE_GAME_CENTER
    [[GameCenterManager sharedManager] saveAndReportScore:(int)highScore
                                              leaderboard:ETGCHighScoreLeaderboard
                                                sortOrder:GameCenterSortOrderHighToLow];
#endif

    [self setValue:@(highScore) forKey:ETGSHighScore];
}

- (void)setPlayTime:(NSInteger)playTime
{
    if (_playTime == playTime)
        return;
    _playTime = playTime;
    [self setValue:@(playTime) forKey:ETGSPlayTime];
}

- (void)setRated:(BOOL)rated
{
    if (_rated == rated)
        return;
    _rated = rated;
    [self setValue:@(rated) forKey:ETGSRated];
}

- (void)setSound:(BOOL)sound
{
    if (_sound == sound)
        return;
    _sound = sound;
    [self setValue:@(sound) forKey:ETGSSound];
}

- (void)setAds:(BOOL)ads
{
    if (_ads == ads)
        return;
    _ads = ads;
    [self setValue:@(ads) forKey:ETGSAds];
}

- (void)setLineBet:(NSInteger)lineBet
{
    if (_lineBet == lineBet)
        return;
    _lineBet = lineBet;
    [self setValue:@(lineBet) forKey:ETGSLineBet];
}


@end
