//
//  ETNSValue+Category.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETNSValue+Category.h"

@implementation NSValue(ETTable)

- (ETTable)ETTableValue
{
    ETTable h; [self getValue: &h]; return h;
}

+ (NSValue *)valueWithETTable:(ETTable)table
{
    return [NSValue valueWithBytes: &table objCType:@encode( ETTable )];
}

- (ETTableCollection)ETTableCollectionValue
{
    ETTableCollection h; [self getValue: &h]; return h;
}

+ (NSValue *)valueWithETTableCollection:(ETTableCollection)tableCollection
{
    return [NSValue valueWithBytes: &tableCollection objCType:@encode( ETTableCollection )];
}

@end

