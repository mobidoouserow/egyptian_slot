//
//  ETConfig.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/5/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#ifndef ETConfig_h
#define ETConfig_h

// in app purchase
static NSString *const ETIAP_20kCoins  = @"com2.elevenm.coin_20000";
static NSString *const ETIAP_55kCoins  = @"com2.elevenm.coin_55000";
static NSString *const ETIAP_120KCoins = @"com2.elevenm.coin_120000";
static NSString *const ETIAP_300KCoins = @"com2.elevenm.coin_300000";
static NSString *const ETIAP_1MCoins   = @"com2.elevenm.coin_1000000";
static NSString *const ETIAP_2p5MCoins = @"com2.elevenm.coin_2500000";

// game center
static NSString *const ETGCHighScoreLeaderboard = @"com.elevenm.leaderboard";
/*Enable game center 1, disable 0*/
#define K_ENABLE_GAME_CENTER 1

// chartboost
static NSString *const ETChartboostAppId = @"56e93c00346b5222ef3180fa";
static NSString *const ETChartboostAppSignature = @"7da69be3b198e602946619fcc55e22efabd98a9b";

// admob
static NSString *const ETAdMobVideoRewardId = @"ca-app-pub-7987998023990709/2697361874";

// other
static NSString *const ETFontName = @"SaranaiGame-Bold";
static NSString *const ETAppLink = @"https://www.google.com/";

/*by default rate app message should pop up after 10 spin */
#define K_RATE_APP_AFTER_SPIN_COUNT 10

/*default credit balance http://prntscr.com/agb1wj*/
#define K_DEFAULT_CREDIT_BALANCE 200000.0

/*Helper button to change symbol appear percent, top right corner http://prntscr.com/agb0ud 
 don't forget to set this value to 0 when uploading to app store */
#define K_CHANGE_SYMBOL_PERCENT 1

#define K_BONUS_GAME_MIN 100

#define K_BONUS_GAME_MAX 1000


#endif /* ETConfig_h */
