//
//  ETButton.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETButton.h"
#import "CCControlSubclass.h"

#define kCCFatFingerExpansion 70


@implementation ETButton

- (void)updatePropertiesForState:(CCControlState)state
{
    self.background.color = [self backgroundColorForState:state];
    self.background.opacity = [self backgroundOpacityForState:state];
    
    CCSpriteFrame* spriteFrame = [self backgroundSpriteFrameForState:state];
    if (!spriteFrame) spriteFrame = [self backgroundSpriteFrameForState:CCControlStateNormal];
    self.background.spriteFrame = spriteFrame;
    
    self.label.color = [self labelColorForState:state];
    self.label.opacity = [self labelOpacityForState:state];
    
    [self needsLayout];
}

- (void)stateChanged
{
    if (self.enabled)
    {
        if (self.highlighted)
        {
            [self updatePropertiesForState:CCControlStateHighlighted];
            if (self.zoomWhenHighlighted)
            {
                [self.background stopAllActions];
                [self.label runAction:[CCActionScaleTo actionWithDuration:0.1 scaleX:_originalScaleX*0.9 scaleY:_originalScaleY*0.9]];
                if (!CGPointEqualToPoint(_originalLabelPosition, CGPointZero))
                    [self.label runAction:[CCActionMoveTo actionWithDuration:0.1 position:ccpSub(self.originalLabelPosition, ccp(1,1)) ]];
                
                [self.background runAction:[CCActionScaleTo actionWithDuration:0.1 scaleX:_originalScaleX*0.9 scaleY:_originalScaleY*0.9]];
            }
        }
        else
        {
            if (self.selected)
            {
                [self updatePropertiesForState:CCControlStateSelected];
            }
            else
            {
                [self updatePropertiesForState:CCControlStateNormal];
            }
            
            [self.label stopAllActions];
            if (self.zoomWhenHighlighted)
            {
                self.label.scaleX = _originalScaleX;
                self.label.scaleY = _originalScaleY;
                if (!CGPointEqualToPoint(_originalLabelPosition, CGPointZero))
                    self.label.position = _originalLabelPosition;
                
                [self.background runAction:[CCActionScaleTo actionWithDuration:0.1 scaleX:_originalScaleX scaleY:_originalScaleY]];
            }
        }
    }
    else
    {
        [self updatePropertiesForState:CCControlStateDisabled];
    }
}

- (void) layout
{
    self.label.dimensions = CGSizeZero;
    CGSize originalLabelSize = self.label.contentSize;
    CGSize paddedLabelSize = originalLabelSize;
    paddedLabelSize.width += self.horizontalPadding * 2;
    paddedLabelSize.height += self.verticalPadding * 2;
    
    BOOL shrunkSize = NO;
    CGSize size = [self convertContentSizeToPoints: self.preferredSize type:self.contentSizeType];
    
    CGSize maxSize = [self convertContentSizeToPoints:self.maxSize type:self.contentSizeType];
    
    if (size.width < paddedLabelSize.width) size.width = paddedLabelSize.width;
    if (size.height < paddedLabelSize.height) size.height = paddedLabelSize.height;
    
    if (maxSize.width > 0 && maxSize.width < size.width)
    {
        size.width = maxSize.width;
        shrunkSize = YES;
    }
    if (maxSize.height > 0 && maxSize.height < size.height)
    {
        size.height = maxSize.height;
        shrunkSize = YES;
    }
    
    if (shrunkSize)
    {
        CGSize labelSize = CGSizeMake(clampf(size.width - self.horizontalPadding * 2, 0, originalLabelSize.width),
                                      clampf(size.height - self.verticalPadding * 2, 0, originalLabelSize.height));
        self.label.dimensions = labelSize;
    }
    
    self.background.contentSize = size;
    self.background.anchorPoint = ccp(0.5f,0.5f);
    self.background.positionType = CCPositionTypeNormalized;
    self.background.position = ccp(0.5f,0.5f);
    
    if (!self.freeLabelPosition)
    {
        self.label.positionType = CCPositionTypeNormalized;
        self.label.position = ccp(0.5f, 0.5f);
    }
    
    self.contentSize = [self convertContentSizeFromPoints: size type:self.contentSizeType];
    
    _needsLayout = NO;
    //[super layout];
}





@end
