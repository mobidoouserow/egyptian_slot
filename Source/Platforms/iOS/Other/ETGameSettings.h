//
//  ETGameSettings.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ETGameSettings : NSObject

+ (instancetype)defaultSettings;

@property (nonatomic, assign)NSInteger lines;

@property (nonatomic, assign)CGFloat credits;

@property (nonatomic, assign)CGFloat highScore;

@property (nonatomic, assign)NSInteger playTime;

@property (nonatomic, assign)BOOL rated;

@property (nonatomic, assign)BOOL sound;

@property (nonatomic, assign)BOOL ads;

@property (nonatomic, assign)NSInteger lineBet;

@property (nonatomic, strong)NSMutableDictionary *setting;

@end
