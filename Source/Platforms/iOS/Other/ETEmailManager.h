//
//  ETEmailManager.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@protocol ETEmailManagerDelegate <NSObject>

@optional
- (void)mailComposeControllerWillPresent;

- (void)mailComposeControllerDidFinishWithResult:(MFMailComposeResult)result;

- (void)networkNotReachable;

@end

@interface ETEmailManager : NSObject <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, weak)id <ETEmailManagerDelegate> delegate;

@property (nonatomic, strong)NSString *title;

@property (nonatomic, strong)NSString *body;

@property (nonatomic, strong)NSArray *recipient;

@property (nonatomic, strong)MFMailComposeViewController *picker;

- (instancetype)initWithRecipient:(NSArray *)recipient title:(NSString *)title body:(NSString *)body;

+ (instancetype)emailWithRecipient:(NSArray *)recipient title:(NSString *)title body:(NSString *)body;

- (void)showWithAttachment:(NSData *)data mimeType:(NSString *)mimeType fileName:(NSString *)fileName;

- (void)show;


@end
