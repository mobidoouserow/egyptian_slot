//
//  ETStoreManager.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETStoreManager.h"

@interface ETStoreManager ()
@property (nonatomic, strong)NSMutableSet *requestingProducts;
@property (nonatomic, strong)NSMutableDictionary *validProducts;
@end

@implementation ETStoreManager

+ (instancetype)sharedStoreManager
{
    static ETStoreManager *singleton;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singleton = [[ETStoreManager alloc] init];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:singleton];
    });
    
    return singleton;
}

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    _requestingProducts = [[NSMutableSet alloc] init];
    _validProducts = [[NSMutableDictionary alloc] init];
    
    return self;
}

- (void)startWithProducts:(NSSet *)products delegate:(id <ETStoreDelegate>)delegate
{
    if ([SKPaymentQueue canMakePayments] == NO)
        return;
    
    self.delegate = delegate;
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:products];
    request.delegate = self;
    [request start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    for (SKProduct *product in response.products)
    {
        [_validProducts setValue:product forKey:product.productIdentifier];
        
        if ([_requestingProducts containsObject:product.productIdentifier])
        {
            [_requestingProducts removeObject: product.productIdentifier];
            [self purchaseProduct:product];
        }
        
        if ([self.delegate respondsToSelector:@selector(didReceiveResponseForProduct:)])
            [self.delegate didReceiveResponseForProduct:product];
    }
}

- (void)restore
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [self alertWithTitle:@"Restore Failed" message:error.localizedDescription];
}

- (SKProduct *)productForIdentifier:(NSString *)identifier
{
    for (NSString *key in _validProducts)
    {
        if ([key isEqualToString:identifier] == NO)
            continue;
        
        SKProduct *product = _validProducts[identifier];
        return product;
    }
    
    //NSLog(@"Warning Can't find product with Id: %@", identifier);
    return nil;
}

- (void)purchase:(NSString *)identifier
{
    if ([SKPaymentQueue canMakePayments])
    {
        if ([_requestingProducts containsObject: identifier])
            return;
        
        SKProduct *product = [self productForIdentifier:identifier];
        if (product != nil)
            [self purchaseProduct: product];
        else
            [_requestingProducts addObject: identifier];
    }
    else
    {
        [self alertWithTitle:@"Warning" message:@"Purchases are disabled on this device."];
    }
}

- (void)purchaseProduct:(SKProduct *)product
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (NSString *)localPriceForProduct:(NSString *)identifier
{
    SKProduct *product = [self productForIdentifier:identifier];
    if (product != nil)
        return [self localPrice: product];
    return @"";
}

- (NSString *)localPrice:(SKProduct *)product
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.locale = product.priceLocale;
    
    return [formatter stringFromNumber:product.price];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased: {
                [self purchaseCompleted:transaction]; break;
            }
            case SKPaymentTransactionStateRestored: {
                [self purchaseRestored:transaction]; break;
            }
            case SKPaymentTransactionStateFailed: {
                [self purchaseFailed:transaction]; break;
            }
            default: break;
        }
    }
}

- (void)purchaseCompleted:(SKPaymentTransaction *)transaction
{
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if ([self.delegate respondsToSelector:@selector(transactionSuccessfulForIdentifier:)])
        [self.delegate transactionSuccessfulForIdentifier:transaction.payment.productIdentifier];
    
    [self alertWithTitle:@"Purchase Status" message:@"Your transaction has been processed successfully."];
}

- (void)purchaseRestored:(SKPaymentTransaction *)transaction
{
    [self purchaseCompleted:transaction];
}

- (void)purchaseFailed:(SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled)
        [self alertWithTitle:@"Purchase Status" message:@"Your transaction failed"];
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if ([self.delegate respondsToSelector:@selector(transactionFailedForIdentifier:)])
        [self.delegate transactionFailedForIdentifier:transaction.payment.productIdentifier];
}

-(void)alertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert =   [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alert addAction:ok];
    
    [[CCDirector sharedDirector] presentViewController:alert animated:YES completion:nil];
}



@end
