//
//  ETActionSlot.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETActionSlot.h"
#import "ETReelParalax.h"
#import "ETGameSymbol.h"

@implementation ETActionSlotParallax

+ (instancetype)actionWithDuration:(CCTime)t velocity:(CGPoint)velocity rate:(float)rate
{
    return [[self alloc] initWithDuration:t velocity:velocity rate:rate];
}

- (instancetype)initWithDuration:(CCTime)t velocity:(CGPoint)velocity rate:(float)rate
{
    if( (self = [super initWithDuration:t]) )
    {
        _velocity = velocity;
        _rate = rate;
        _dt = 0.07;
    }
    return self;
}


- (void)update:(CCTime)t
{
    CGFloat frac = fmodf( t * 1, 1.0f );
    CGFloat y = 1 * 4 * frac * (1 - frac);
    CGPoint velocity = ccpMult(_velocity, y);
    
    ETReelParalax *node = (ETReelParalax *)_target;
    CGPoint step = ccpMult(velocity, _dt);
    node.position = ccpAdd(node.position, step);
    
    for (CGPointObject *point in node.parallaxArray)
    {
        ETGameSymbol *symbol = (ETGameSymbol *)point.child;
        CGPoint position = [node convertToWorldSpace:symbol.position];
        CGPoint end = [node.parent convertToWorldSpace:ccp(0.0, -symbol.tileSize.height * 0.25)];
        if ( position.y <  end.y)
        {
            CGPoint viewSize = ccp(0.0, symbol.tileSize.height * 4 );
            if (symbol.replaced == NO)
            {
                point.offset = ccpAdd(point.offset, viewSize);
                [symbol changeType];
                [symbol updateCellWithOffset:point.offset];
            }
            symbol.replaced = YES;
            break;
        }
        else
            symbol.replaced = NO;
    }
}

@end

