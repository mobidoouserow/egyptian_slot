//
//  ETAnimationManager.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ETAnimationManager : NSObject

+ (instancetype)sharedAnimationManager;

- (void)cacheSpriteFramesFromFiles:(NSSet *)files metadata:(NSString *)metadata;

- (CCAnimation *)animationByName:(NSString *)animationName;


@end
