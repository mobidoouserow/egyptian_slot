//
//  ETPathHelper.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@interface ETPathHelper : CCNode

- (NSArray *)findWinLine:(NSArray *)grid lines:(NSInteger)lines;

- (NSInteger)scatterCount:(NSArray *)grid;


@end
