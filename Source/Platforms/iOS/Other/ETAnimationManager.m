//
//  ETAnimationManager.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETAnimationManager.h"
#import "CCAnimationCache.h"

static NSString *const d_from  = @"from";
static NSString *const d_to    = @"to";

@implementation ETAnimationManager

+ (instancetype)sharedAnimationManager
{
    static ETAnimationManager *singleton;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        singleton = [[ETAnimationManager alloc] init];
    });
    
    return singleton;
}

- (void)cacheSpriteFramesFromFiles:(NSSet *)files metadata:(NSString *)metadata
{
    [files enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, BOOL * _Nonnull stop) {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:obj];
    }];
    
    [self cacheAnimations:metadata];
}

- (void)cacheAnimations:(NSString *)data
{
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:data];
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    CGFloat delay = 1.0 / K_ANIMATION_FRAMES_IN_SEC;
    
    __weak typeof(self)_self = self;
    [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString *_Nonnull frameName, NSDictionary *_Nonnull data, BOOL * _Nonnull stop) {
        NSArray *frames = [_self animationWithFrameName:frameName indexFrom:[data[d_from] integerValue] to:[data[d_to] integerValue]];
        [[CCAnimationCache sharedAnimationCache] addAnimation:[CCAnimation animationWithSpriteFrames:frames delay:delay] name:frameName];

    }];
}

- (NSArray *)animationWithFrameName:(NSString *)frameName indexFrom:(NSInteger)from to:(NSInteger)to
{
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = from; i <= to; i++)
    {
        NSString *imageName = [frameName stringByAppendingFormat:@"_%.02li.png",(long)i];
        [array addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: imageName]];
    }
    return array;
}

- (NSString *)animationNameForObject:(NSString *)object withSuffix:(NSString *)suffix
{
    return [object stringByAppendingFormat:@"_%@",suffix];
}

- (CCAnimation *)animationByName:(NSString *)animationName
{
    return [[CCAnimationCache sharedAnimationCache] animationByName:animationName];
}


@end
