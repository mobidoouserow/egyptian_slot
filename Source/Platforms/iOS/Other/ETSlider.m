//
//  ETSlider.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETSlider.h"
#import "CCControlSubclass.h"

@implementation ETSlider {
    BOOL _continueTouch;
}

- (instancetype)initWithBackground:(CCSpriteFrame *)background andHandleImage:(CCSpriteFrame *)handle max:(double)max
{
    self = [super initWithBackground:background andHandleImage:handle];
    if (!self)return nil;
    
    _min = 0;
    _max = max;
    
    self.value = 0;
    self.continuous = YES;
    
    _label = [self valueLabel];
    [self.handle addChild:_label];
    
    return self;
}

- (instancetype)initWithMax:(double)max min:(double)min value:(double)value
{
    self = [super initWithBackground:[CCSpriteFrame frameWithImageNamed:@"slider_bg.png"]
                      andHandleImage:[CCSpriteFrame frameWithImageNamed:@"slider_handle.png"]];
    
    if (!self)return nil;
    
    _max = max;
    _min = min;
    self.value = value;
    _label = [self valueLabel];
    [self.handle addChild:_label];
    self.continuous = YES;
    self.endStop = self.handle.contentSize.width * 0.5;
    
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    _beganDrag = self.sliderValue;
    if (_touchBlock)
    {
        if (_touchBlock(self, touch))
            [super touchBegan:touch withEvent:event];
    }
    else
        [super touchBegan:touch withEvent:event];
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    if (_touchBlock)
    {
        if (_touchBlock(self, touch))
            [super touchMoved:touch withEvent:event];
    }
    else
        [super touchMoved:touch withEvent:event];
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    if (_touchBlock)
    {
        if (_touchBlock(self, touch))
            [super touchEnded:touch withEvent:event];
    }
    else
        [super touchEnded:touch withEvent:event];
}

- (CCLabelTTF *)valueLabel
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:[self valueToString:_value]
                                           fontName:ETFontName
                                           fontSize:ETUniversalValue(30)];
    
    label.positionType = CCPositionTypeNormalized;
    label.anchorPoint = ccp(0.5, 0.0);
    label.position = ccp(0.5, 1.1);
    return label;
}

- (void)setValue:(double)value
{
    value = MAX(MIN(value, _max), _min);
    if (_value == value)
        return;
    _value = value;
    
    if (_label)
        _label.string = [self valueToString:value];
    
    double slider = (value - _min) / (_max - _min);
    slider = round (slider * 100) / 100.0;
    self.sliderValue = slider;
}

- (NSString *)valueToString:(double)value
{
    NSString *string = nil;
    if (_floatValue)
        string = [NSString stringWithFormat:@"%.2f", value];
    else
        string = [NSString stringWithFormat:@"%li", (long)value];
    return string;
}

- (void)setFloatValue:(BOOL)floatValue
{
    if (_floatValue == floatValue)
        return;
    _floatValue = floatValue;
    _label.string = [self valueToString:_value];
}

- (void)setSliderValue:(float)sliderValue
{
    _dragStartValue = sliderValue;
    [super setSliderValue:sliderValue];
}

- (void)triggerAction
{
    if (!self.enabled)
        return;
    
    _preValue = _value;
    _value = (_max - _min) * self.sliderValue + _min;
    _value = round (_value * 100) / 100.0;
    _label.string = [self valueToString:_value];
    
    [super triggerAction];
}

- (float)deltaValue
{
    return _value - _preValue;
}



@end
