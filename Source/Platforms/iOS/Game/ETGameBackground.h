//
//  ETGameBackground.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@interface ETGameBackground : CCNode

@property (nonatomic, assign)ETDevice device;

@property (nonatomic, strong)CCNode *symbolPanel;

@property (nonatomic, readonly)CGPoint symbolsPosition;

@end
