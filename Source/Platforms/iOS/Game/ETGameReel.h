//
//  ETGameReel.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"
#import "ETGameSymbol.h"

@interface ETGameReel : CCNode <ETGameSymbollDelegate>

@property (nonatomic, assign)NSInteger reelRow;

@end
