//
//  ETGameSymbol.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameSymbol.h"
#import "CCAnimation.h"
#import "CCAnimationCache.h"
#import "AppDelegate.h"

static NSString *const ETFrameContour = @"FrameContour";
#define ETSymbolAnimationTag 1

@interface ETGameSymbol ()
@property (nonatomic, strong)CCSprite *symbolFrame;

@end

@implementation ETGameSymbol

+ (instancetype)randomSymbolOnCell:(ETTable)cell delegate:(id <ETGameSymbollDelegate>)delegate
{
    return [[self alloc] initWithSymbolType:[[AppController sharedAppController] smartRandomSymbol] cell:cell delegate:delegate];
}

+ (instancetype)symbolWithType:(ETSymbolType)type cell:(ETTable)cell delegate:(id <ETGameSymbollDelegate>)delegate
{
    return [[self alloc] initWithSymbolType:type cell:cell delegate:delegate];
}

- (instancetype)initWithSymbolType:(ETSymbolType)type cell:(ETTable)cell delegate:(id<ETGameSymbollDelegate>)delegate
{
    self = [super initWithImageNamed:[self symbolNameForType:type]];
    if (!self) return(nil);
    
    _type = type;
    _delegate = delegate;
    _cell = cell;
 
/*  //Debug
    _label = [self createCellLabel:cell];
    [self addChild:_label];
 */
    
    _symbolFrame = [self createSymbolFrame];
    [self addChild:_symbolFrame z:-1];
    
    return self;
}

- (NSString *)symbolNameForType:(ETSymbolType)type
{
    NSAssert(type != ETSymbolNone, @"Symbol Type is None: %lu", (unsigned long)type);
    return [NSString stringWithFormat:@"symbol%lu_00.png", (unsigned long)type];
}

- (CCLabelTTF *)createCellLabel:(ETTable)cell
{
    CCLabelTTF * label = [CCLabelTTF labelWithString:NSStringFromETTable(cell) fontName:ETFontName fontSize:ETUniversalValue(14)];
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.5,0.5);
    return label;
}

+ (ETSymbolType)randomSymbol
{
    return  arc4random_uniform(ETSymbol12) + 1;
}

- (CGPoint)calculatePositionForCell:(ETTable)cell
{
    return ccp(cell.row * _tileSize.width + _tileSize.width * 0.5, cell.column * _tileSize.height + _tileSize.height * 0.5 );
}

- (CGPoint)calculateHighestPosition
{
    return ccp(_cell.row * _tileSize.width + _tileSize.width * 0.5, (ETSlotColumnCount-1) * _tileSize.height + _tileSize.height * 0.5 );
}

- (NSString *)animationName
{
    return [NSString stringWithFormat:@"symbol%lu",(unsigned long)(_type)];
}

- (void)changeType
{
    _type = [[AppController sharedAppController] smartRandomSymbol];
    self.spriteFrame = [CCSpriteFrame frameWithImageNamed:[self symbolNameForType:_type]];
}

- (void)setSwitchSymbolFrame:(BOOL)switchSymbolFrame
{
    if (_switchSymbolFrame == switchSymbolFrame)
        return;
    _switchSymbolFrame = switchSymbolFrame;

    _symbolFrame.spriteFrame = [CCSpriteFrame frameWithImageNamed:@"Frames back.png"];
}

- (void)setSymbolFrameContour:(NSInteger)symbolFrameContour
{
    if (_symbolFrame == nil)
        return;
    _symbolFrameContour = symbolFrameContour;
    
    if (_symbolFrame == nil)
        return;
    
    NSString *imageNamed = [NSString stringWithFormat:@"Frame%.02li.png", (long)symbolFrameContour];
    CCSprite * contour = (CCSprite *)[_symbolFrame getChildByName:ETFrameContour recursively:YES];
    contour.spriteFrame = [CCSpriteFrame frameWithImageNamed:imageNamed];
    
    _symbolFrame.visible = YES;
    
    self.animate = YES;
    
}

- (void)updateCellWithOffset:(CGPoint)offset
{
    _cell = ETTableMake(ETSlotColumnCount - 1, _cell.row);
    _label.string = NSStringFromETTable(_cell);
    
    __weak typeof(self)weakSelf = self;
    [self.parent.children enumerateObjectsUsingBlock:^(ETGameSymbol *obj, NSUInteger idx, BOOL *stop) {
        if (obj != weakSelf)
        {
            obj.cell = ETTableMake(MAX(obj.cell.column - 1, 0), obj.cell.row);
            if (obj.label)
                obj.label.string = NSStringFromETTable(obj.cell);
        }
    }];
}

- (CCSprite *)createSymbolFrame
{
    CCSprite *frame = [self spriteWithImageNamed:@"Frames back.png"];
    frame.visible = NO;
    [frame addChild:[self frameContour]];
    
    return frame;
}

- (CCSprite *)frameContour
{
    CCSprite *contour = [self spriteWithImageNamed:@"Frame01.png"];
    contour.name = ETFrameContour;
    return contour;
}

- (CCSprite *)spriteWithImageNamed:(NSString *)imageName
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:imageName];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    return sprite;
}

- (void)setAnimate:(BOOL)animate
{
    if (_animate == animate)
        return;
    _animate = animate;

    if (animate)
    {
        if ([self getActionByTag:ETSymbolAnimationTag])
            return;
        
        CCAnimation *animation = [[CCAnimationCache sharedAnimationCache] animationByName:[self animationName]];
        CCActionAnimate *animate = [CCActionAnimate actionWithAnimation: animation];
        CCAction *action = [CCActionRepeatForever actionWithAction:animate];
        action.tag = ETSymbolAnimationTag;
        
        [self runAction: action];
    }
    else
    {
        if (![self getActionByTag:ETSymbolAnimationTag])
            return;
        
        [self stopActionByTag:ETSymbolAnimationTag];
        self.spriteFrame = [CCSpriteFrame frameWithImageNamed:[self symbolNameForType:_type]];
    }
}

- (void)hideSymbolFrame
{
    if (_symbolFrame.visible == NO)
        return;
    _symbolFrame.visible = NO;
    self.animate = NO;
}


@end
