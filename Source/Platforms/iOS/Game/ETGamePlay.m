//
//  ETGamePlay.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGamePlay.h"
#import "ETGameReelsFrame.h"
#import "ETGameLines.h"
#import "ETGameScene.h"
#import "ETGameBackground.h"
#import "ETGameHud.h"

#import "AppDelegate.h"

@interface ETGamePlay () <ETGameReelsFrameDelegate>
@property (nonatomic, strong)ETGameReelsFrame *reelsFrame;
@end

@implementation ETGamePlay

- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate
{
    self = [super init];
    if (!self) return(nil);
    
    _contentSize = [CCDirector sharedDirector].viewSize;
    _delegate = delegate;
    
    _reelsFrame = [self createReelsFrame];
    [self addChild:_reelsFrame];
    
    return self;
}

- (ETGameReelsFrame *)createReelsFrame
{
    ETGameReelsFrame *frame = [[ETGameReelsFrame alloc] initWithDelegate:self];
    frame.lines.reelsEnable = YES;
    frame.position = ((ETGameScene *)_delegate).gameBackground.symbolsPosition;
    
    return frame;
}

- (void)setLine:(NSInteger)line
{
    if (_line == line)
        return;
    
    [_reelsFrame clean];
    _line = line;
    _reelsFrame.lines.line = line;
}


- (void)spin
{
    [_reelsFrame spin];
}

#pragma mark ETGameReelsFrameDelegate Delegate

- (void)winWithMultiplier:(NSInteger)multiplier
{
    ETGameHud *gameHud = [(ETGameScene *)_delegate gameHud];
    CGFloat win = MAX(1, gameHud.betLine * ((CGFloat)multiplier));
    gameHud.win = win;
    AppController *controller = [AppController sharedAppController];
    
    if (win >= gameHud.totalBet * 3)
    {
        [controller playEffect:@"big_win.mp3"];
        [controller playEffect:@"medium_win.mp3"];
        NSString *applause = [NSString stringWithFormat:@"big_applause_%li.mp3", (long)arc4random_uniform(1)+1];
        id<ALSoundSource> effect = [controller playEffect:applause loop:NO];
        effect.volume = 0.8;
    }
    else
    {
        [controller playEffect:@"medium_win.mp3"];
        NSString *applause = [NSString stringWithFormat:@"medium_applause_%li.mp3", (long)arc4random_uniform(1)+1];
        id<ALSoundSource> effect = [controller playEffect:applause loop:NO];
        effect.volume = 0.3;
    }
    
    NSInteger count = MIN(MAX(100, 100 * (win / gameHud.totalBet) / 2), 2000);
    [_delegate particle: count];
}

- (void)bonusGame:(NSInteger)scatterCount
{
    [_delegate bonus:scatterCount];
}

- (void)lineLost
{
    [[(ETGameScene *)_delegate gameHud] linesLost];
}

- (void)spinStarted
{
    ETGameHud *gameHud = [(ETGameScene *)_delegate gameHud];
    gameHud.enablePanel = NO;
    _reelsFrame.lines.reelsEnable = NO;
    [[AppController sharedAppController]playEffect:@"spin_start.mp3"];
}

- (void)spinEnded:(NSInteger)winLineCount bonusGame:(BOOL)bonusGame
{
    [[AppController sharedAppController] playEffect:@"spin_stop.mp3"];

    ETGameHud *gameHud = [(ETGameScene *)_delegate gameHud];
    gameHud.spinStarted = NO;
    
    if (gameHud.autoStart && !bonusGame)
    {
        if (winLineCount != 0)
            [self runAction: [CCActionSequence actions: [CCActionDelay actionWithDuration:3.0],
                              [CCActionCallFunc actionWithTarget:gameHud selector:@selector(onSpin)], nil]];
        else
            [gameHud onSpin];
    }
    else
    {
        gameHud.enablePanel = YES;
        _reelsFrame.lines.reelsEnable = YES;
    }
}

- (void)setReelLine:(NSInteger)line
{
    ((ETGameScene *)_delegate).gameHud.lines = line;
}


@end
