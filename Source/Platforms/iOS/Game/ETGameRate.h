//
//  ETGameRate.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/12/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@interface ETGameRate : CCNode
- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate;
@end
