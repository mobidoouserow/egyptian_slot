//
//  ETGameLines.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameLines.h"
#import "ETNumbers.h"
#import "AppDelegate.h"

@interface ETGameLines ()
@property (nonatomic, assign)CGSize bounds;
@property (nonatomic, strong)CCNode *frame;
@property (nonatomic, strong)CCLayoutBox *numberLayout;
@property (nonatomic, strong)CCNode *linesNode;
@end


@implementation ETGameLines

- (instancetype)initWithFrame:(CCNode *)frame
{
    self = [super init];
    if (!self) return(nil);
    
    _bounds = [CCDirector sharedDirector].viewSize;
    _frame = frame;
    _reelsEnable = YES;
    
    [self elements];
    
    return self;
}

- (void)elements
{
    _numberLayout = [self numbers];
    [self addChild: _numberLayout];
    
    self.line = [ETGameSettings defaultSettings].lines;
    
    _linesNode = [self lines];
    [self addChild: _linesNode z:-1];
}

- (CCLayoutBox *)numbers
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    NSArray *array = @[ @[ @5,  @3, @17, @11, @1, @10, @16, @20,  @2,  @4],
                        @[@15, @13,  @8, @19, @7,  @6, @18,  @9, @12, @14] ];
    
    for (NSArray *arr in array)
        [layout addChild:[self numbersFromArray:arr]];
    
    NSInteger spacing = ETUniversalValue(3);
    spacing -= (spacing % 2 == 0) ? 0 : 2;
    layout.anchorPoint =  ccp(0.5,0.5);
    layout.position = ccp(_frame.contentSize.width * 0.502, _frame.contentSize.height * 0.48);
    layout.direction = CCLayoutBoxDirectionHorizontal;
    layout.spacing = _frame.contentSize.width + spacing;
    
    return layout;
}

- (CCLayoutBox *)numbersFromArray:(NSArray *)array
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    for (NSNumber *number in array)
    {
        [layout addChild:[self divider]];
        [layout addChild:[self createButtonWithNumber:[number integerValue]]];
    }
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCSprite *)divider
{
    return [CCSprite spriteWithImageNamed:@"divider.png"];
}

- (ETNumbers *)createButtonWithNumber:(NSInteger)number
{
    CCSpriteFrame *normal  = [CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"reels%lu_1.png", (unsigned long)number]];
    CCSpriteFrame *disable = [CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"reels%lu_2.png", (unsigned long)number]];
    
    ETNumbers *button = [ETNumbers buttonWithTitle:@""
                                       spriteFrame:normal
                            highlightedSpriteFrame:normal
                               disabledSpriteFrame:disable];
    
    button.name = [NSString stringWithFormat:@"%lu", (unsigned long)number];
    [button setTarget:self selector:@selector(onNumber:)];
    
    return button;
}

- (CCNode *)lines
{
    CCNode *node = [[CCNode alloc] init];
    NSInteger lineCount = [AppController sharedAppController].winLines.count;
    for (NSInteger i = 0; i < lineCount; i++)
    {
        CCSprite *line = [CCSprite spriteWithImageNamed:[self lineImageName:i+1]];
        line.position = [self linePositionForNumber:i+1];
        line.name = [NSString stringWithFormat:@"%lu", (long)i+1];
        line.visible = NO;
        [node addChild: line];
    }
    
    return node;
}

- (NSString *)lineImageName:(NSInteger)lineNumber
{
    return [NSString stringWithFormat:@"Line%lu.png", (long)lineNumber];
}

- (void) updatePropertiesForState:(CCControlState)state button:(CCButton *)button
{
    CCSpriteFrame* spriteFrame = [button backgroundSpriteFrameForState:state];
    button.background.spriteFrame = spriteFrame;
}

- (void)setLine:(NSInteger)line
{
    if (_line == line)
        return;
    _line = line;
    [ETGameSettings defaultSettings].lines = line;
    
    [_numberLayout.children enumerateObjectsUsingBlock:^(CCLayoutBox *layout, NSUInteger idx1, BOOL *stop1) {
        [layout.children enumerateObjectsUsingBlock:^(ETNumbers *number, NSUInteger idx2, BOOL *stop2) {
            if ([number isKindOfClass:[ETNumbers class]])
            {
                BOOL reelEnable = [number.name integerValue] <= _line;
                if (number.numberEnable != reelEnable)
                    number.numberEnable = reelEnable;
            }
        }];
    }];
    
    [_linesNode.children enumerateObjectsUsingBlock:^(CCSprite *line, NSUInteger idx, BOOL *stop) {
        NSInteger number = [line.name integerValue];
        BOOL visible = number <= _line;
        if (line.visible != visible)
            line.visible = visible;
    }];
}

- (void)hideAllLines
{
    [_linesNode.children enumerateObjectsUsingBlock:^(CCSprite *line, NSUInteger idx, BOOL *stop) {
        if (line.visible != NO) line.visible = NO;
    }];
}

- (void)setStatus:(BOOL)status forLine:(NSInteger)line
{
    CCButton *button = (CCButton *)[_numberLayout getChildByName:[NSString stringWithFormat:@"%lu", (unsigned long)line] recursively:YES];
    
    CCControlState state = status ? CCControlStateNormal : CCControlStateDisabled;
    [self updatePropertiesForState:state button:button];
}

- (CGPoint)linePositionForNumber:(NSInteger)number
{
    
    CGSize size = _frame.contentSize;
    CGPoint point = CGPointZero;
    
    switch (number)
    {
        case 1:  point = ccp(size.width * 0.5, size.height * 0.44); break;
        case 2:  point = ccp(size.width * 0.5, size.height * 0.84); break;
        case 3:  point = ccp(size.width * 0.5, size.height * 0.142); break;
        case 4:  point = ccp(size.width * 0.5, size.height * 0.545); break;
        case 5:  point = ccp(size.width * 0.5, size.height * 0.44); break;
        case 6:  point = ccp(size.width * 0.5, size.height * 0.72); break;
        case 7:  point = ccp(size.width * 0.5, size.height * 0.265); break;
        case 8:  point = ccp(size.width * 0.5, size.height * 0.49); break;
        case 9:  point = ccp(size.width * 0.5, size.height * 0.52); break;
        case 10: point = ccp(size.width * 0.5, size.height * 0.5); break;
        case 11: point = ccp(size.width * 0.5, size.height * 0.49); break;
        case 12: point = ccp(size.width * 0.5, size.height * 0.65); break;
        case 13: point = ccp(size.width * 0.5, size.height * 0.335); break;
        case 14: point = ccp(size.width * 0.5, size.height * 0.695); break;
        case 15: point = ccp(size.width * 0.5, size.height * 0.29); break;
        case 16: point = ccp(size.width * 0.5, size.height * 0.68); break;
        case 17: point = ccp(size.width * 0.5, size.height * 0.35); break;
        case 18: point = ccp(size.width * 0.5, size.height * 0.56); break;
        case 19: point = ccp(size.width * 0.5, size.height * 0.45); break;
        case 20: point = ccp(size.width * 0.5, size.height * 0.41); break;
        default: break;
    }
    return point;
}

- (void)setLine:(NSInteger)lineNumber visible:(BOOL)visible
{
    CCSprite *sprite = (CCSprite *)[_linesNode getChildByName:[NSString stringWithFormat:@"%lu", (unsigned long)lineNumber] recursively:NO];
    if (sprite.visible != visible)
        sprite.visible = visible;
}

- (void)setReelsEnable:(BOOL)reelsEnable
{
    if (_reelsEnable == reelsEnable)
        return;
    
    _reelsEnable = reelsEnable;
    
    [_numberLayout.children enumerateObjectsUsingBlock:^(CCLayoutBox *layout, NSUInteger idx1, BOOL *stop1) {
        [layout.children enumerateObjectsUsingBlock:^(ETNumbers *number, NSUInteger idx2, BOOL *stop2) {
            if ([number isKindOfClass:[ETNumbers class]])
                number.enabled = reelsEnable;
        }];
    }];
}

- (void)onNumber:(CCButton *)sender
{
    [[AppController sharedAppController] click];
    if ([_delegate respondsToSelector:@selector(setReelLine:)])
        [_delegate setReelLine:[sender.name integerValue]];
}


@end
