//
//  ETGameLines.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"
#import "ETGameReelsFrame.h"

@interface ETGameLines : CCNode

@property (nonatomic, assign)NSInteger line;

@property (nonatomic, weak)id <ETGameReelsFrameDelegate> delegate;

@property (nonatomic, assign)BOOL reelsEnable;

- (instancetype)initWithFrame:(CCNode *)frame;

- (void)setStatus:(BOOL)status forLine:(NSInteger)line;

- (void)setLine:(NSInteger)lineNumber visible:(BOOL)visible;

- (void)hideAllLines;


@end
