//
//  ETGameRate.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/12/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameRate.h"
#import "ETButton.h"
#import "AppDelegate.h"

@interface ETGameRate ()
@property (nonatomic, weak)id <ETEgyptianTreasuresDelegate> delegate;
@end

@implementation ETGameRate

- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate
{
    self = [super init];
    if (!self) return nil;
    
    _contentSize = [CCDirector sharedDirector].viewSize;
    self.userInteractionEnabled = YES;
    _delegate = delegate;
    
    [self addChild:[self background]];
    [self addChild:[self panel]];
    
    self.cascadeOpacityEnabled = YES;
    self.opacity = 0;
    [self runAction: [CCActionFadeIn actionWithDuration:0.3]];
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (CCNodeColor *)background
{
    CCNodeColor *node = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
    [node addChild:[self panel]];
    return node;
}

- (CCSprite *)panel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"win back.png"];
    [sprite addChild:[self layoutPanel:sprite.contentSize]];
    
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    
    sprite.scale = 0.0f;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:0.4 scale:1.0]]
                                                     rate:3.0]];

    return sprite;
}

- (CCLayoutBox *)layoutPanel:(CGSize)size
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self buttons]];
    [layout addChild:[self message:size]];
    [layout addChild:[self title]];
    
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.52);
    layout.spacing = ETUniversalValue(10);
    layout.anchorPoint = ccp(0.5, 0.5);
    
    return layout;
}

- (CCLabelTTF *)title
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Rate App" fontName:ETFontName fontSize:ETUniversalValue(22)];
    label.color = [CCColor colorWithRed:0.427 green:0.255 blue:0.137 alpha:1];
    return label;
}

- (CCLabelTTF *)message:(CGSize)size
{
    size = CGSizeMake(size.width * 0.8, size.height * 0.4);
    NSString *string = @"If you enjoy Egyptian Treasures, would you mind taking a moment to rate it? It won't take more then a minute. Thanks for your support!";
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:ETUniversalValue(12) dimensions:size];
    label.color = [CCColor colorWithRed:0.616 green:0.000 blue:0.000 alpha:1];
    label.horizontalAlignment = CCTextAlignmentCenter;
    label.verticalAlignment = CCVerticalTextAlignmentCenter;
    
    return label;
}

- (CCLayoutBox *)buttons
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self createButtonWithTitle:@"Rate" imageNamed:@"button_empty.png" selector:@selector(onRateApp:)]];
    [layout addChild:[self createButtonWithTitle:@"Later" imageNamed:@"button_empty.png" selector:@selector(onLater:)]];
    [layout addChild:[self createButtonWithTitle:@"No" imageNamed:@"button_empty.png" selector:@selector(onNoThanks:)]];
    
    layout.spacing = ETUniversalValue(8);
    
    return layout;
}

- (ETButton *)createButtonWithTitle:(NSString *)title imageNamed:(NSString *)imageName selector:(SEL)selector
{
    ETButton *button = [ETButton buttonWithTitle:title
                                     spriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:1]]
                          highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:2]]
                             disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:4]]];
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:3]] forState:CCControlStateSelected];
    [button setTarget:self selector:selector];
    
    button.label.fontName = ETFontName;
    button.label.fontSize = ETUniversalValue(12);
    button.zoomWhenHighlighted = YES;
    button.label.anchorPoint = ccp(0.5, 0.35);
    
    return button;
}

- (NSString *)buttonImageName:(NSString *)imageName value:(NSInteger)value
{
    return [NSString stringWithFormat:@"%@%li.%@",[imageName stringByDeletingPathExtension],(long)value, [imageName pathExtension]];
}

- (void)clean
{
    self.cascadeOpacityEnabled = YES;
    [self runAction:[CCActionSequence actionOne:[CCActionFadeOut actionWithDuration:0.3] two:[CCActionRemove action]]];
}

- (void)onRateApp:(id)sender
{
    [[AppController sharedAppController] click];
    [ETGameSettings defaultSettings].rated = YES;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ETAppLink]];
    [self clean];
}

- (void)onLater:(id)sender
{
    [[AppController sharedAppController] click];
    [self clean];
}

- (void)onNoThanks:(id)sender
{
    [[AppController sharedAppController] click];
    [ETGameSettings defaultSettings].rated = YES;
    [self clean];
}

@end
