//
//  ETGameReelsFrame.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameReelsFrame.h"
#import "ETGameSymbol.h"
#import "ETSymbolRules.h"
#import "ETGameLines.h"
#import "ETReelParalax.h"
#import "ETActionSlot.h"
#import "ETPathHelper.h"
#import "AppDelegate.h"

#define ETWinActionTag 1

@interface ETGameReelsFrame () <ETGameSymbollDelegate, ETWinLineDelegate>
@property (nonatomic, strong)NSMutableArray *parrallaxArray;
@property (nonatomic, strong)CCSprite *base;
@property (nonatomic, strong)NSArray *durationArray;
@property (nonatomic, assign)CGSize bounds;
@end

@implementation ETGameReelsFrame

- (instancetype)initWithDelegate:(id <ETGameReelsFrameDelegate>)delegate
{
    self = [super init];
    if (!self) return(nil);
    
    _delegate = delegate;
    _bounds = [CCDirector sharedDirector].viewSize;
    _parrallaxArray = [[NSMutableArray alloc] init];
    _durationArray = @[@2.44, @2.65, @2.85, @3.055, @3.26];
    
    [self clip];
    
    return self;
}

- (void)clip
{
    _base = [self stencilBase];
    [self addChild:_base z:-2];
    
    CCClippingNode *clip = [CCClippingNode clippingNodeWithStencil:_base];
    clip.alphaThreshold = 0.5;
    
    CCSprite *stencil = [self lineStencilWithOpacity:0.0];
    stencil.position = ccp(0, ETUniversalValue(4) );
    [self reelsWithStencil:stencil];
    
    [clip addChild: stencil];
    [self addChild: clip];
    
    CCSprite *contour = [self createFrameContour];
    [self addChild: contour];
    
    _lines = [self createLinesWithFrame:contour];
    [self addChild: _lines z:-1];
    
}

- (CCSprite *)createFrameContour
{
    CCSprite *contour = [self lineStencilWithOpacity:1.0];
//    [contour addChild: [self header]];

    return contour;
}

- (ETGameLines *)createLinesWithFrame:(CCSprite *)frame
{
    ETGameLines *lines = [[ETGameLines alloc] initWithFrame:frame];
    lines.delegate = _delegate;
    lines.position = [frame convertToNodeSpace:ccpNeg(ccpFromSize(frame.contentSize))];
    return lines;
}

- (CCSprite *)header
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"Golde slots ribbon.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 1.015);
    
    return sprite;
}

- (CCSprite *)stencilBase
{
    return [CCSprite spriteWithImageNamed:@"symbols_frame_base.png"];
}

- (CCSprite *)lineStencilWithOpacity:(CGFloat)opacity
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"symbols_frame_contour.png"];
    sprite.opacity = opacity;
    sprite.position = ccp(0.0, -1.0);
    return sprite;
}

- (CGPoint)calculatePositionForCell:(ETTable)cell size:(CGSize)size
{
    return ccp( cell.row * size.width + size.width * 0.5, cell.column * size.height + size.height * 0.5 );
}

- (CGSize)tileSize:(CGSize)size
{
    float height = size.height / (ETSlotColumnCount - 1); // 3
    return CGSizeMake(size.width / ETSlotRowCount, height - height * 0.05);
}

- (void)reelsWithStencil:(CCSprite *)stencil
{
    CGSize size = [self tileSize:stencil.contentSize];
    
    for (NSInteger row = 0; row < ETSlotRowCount; row++)
    {
        ETReelParalax *paralax = [[ETReelParalax alloc] init];
        for (NSInteger column = 0; column < ETSlotColumnCount; column++)
        {
            ETGameSymbol *symbol = [ETGameSymbol randomSymbolOnCell:ETTableMake(column, row) delegate:self];
            symbol.tileSize = size;
            CGPoint point = [self calculatePositionForCell:symbol.cell size:size];
            [paralax addChild:symbol z:0 parallaxRatio:ccp(1.0,1.0) positionOffset:point];
        }
        [stencil addChild: paralax z:-1];
        [_parrallaxArray addObject: paralax];
    }
}

- (void)clean
{
    [self stopActionByTag: ETWinActionTag];
    [self cleanBeforeSpin];
}

- (void)spin
{
    [self clean];
    
    if ([_delegate respondsToSelector:@selector(spinStarted)])
        [_delegate spinStarted];
    
    CGPoint velocity = ccp(0.0, ETUniversalValue(-750));
    NSInteger count = [_parrallaxArray count]-1;
    
    
    __weak typeof(self)weakSelf = self;
    __weak typeof(_durationArray)weakDuration = _durationArray;
    [_parrallaxArray enumerateObjectsUsingBlock:^(ETReelParalax *parallax, NSUInteger idx, BOOL *stop) {
        [parallax runAction: [CCActionSequence actions:
                         [ETActionSlotParallax actionWithDuration:[weakDuration[idx] floatValue] velocity:velocity rate:0.1],
                         [CCActionCallBlock actionWithBlock:^ { [weakSelf getBack:parallax last:idx == count]; }] , nil]];
    }];
}

- (void)cleanBeforeSpin
{
    [_lines hideAllLines];
    [_parrallaxArray enumerateObjectsUsingBlock:^(ETReelParalax *parallax, NSUInteger idx1, BOOL *stop1) {
        [parallax.parallaxArray enumerateObjectsUsingBlock:^(CGPointObject *obj2, NSUInteger idx2, BOOL *stop2) {
            ETGameSymbol *symbol = (ETGameSymbol *)obj2.child;
            [symbol hideSymbolFrame];
            symbol.animate = NO;
        }];
    }];
}

- (void)getBack:(ETReelParalax *)parallax last:(BOOL)last
{
    CGPoint point = [parallax calculatePosition];
    __weak AppController *weakAppController = [AppController sharedAppController];
    __weak typeof(self)weakSelf = self;
    [parallax runAction:[CCActionSequence actions:
                         [CCActionEaseOut actionWithAction:[CCActionMoveTo actionWithDuration:0.3 position:point] rate:0.5],
                         [CCActionCallBlock actionWithBlock:^
                          {
                              [weakAppController playEffect:@"reel_stop.mp3"];
                              if (last) [weakSelf findWinLines:_parrallaxArray];
                          }], nil]];
}

- (void)findWinLines:(NSArray *)array
{
    ETPathHelper *pathHelper = [[ETPathHelper alloc] init];
    NSArray * winLines = [pathHelper findWinLine:array lines:_lines.line];
    NSInteger scatters = [pathHelper scatterCount:array];
    
    BOOL bonusGame = scatters >= 3;
    if (bonusGame)
    {
        if ([_delegate respondsToSelector:@selector(bonusGame:)])
            [_delegate bonusGame:scatters];
    }
    
    if ([winLines count] != 0)
    {
        __block NSInteger multiplier = 0;
        NSMutableArray *actions = [[NSMutableArray alloc] init];
        
        __weak typeof(self)weakSelf = self;
        [winLines enumerateObjectsUsingBlock:^(ETWinLine *obj, NSUInteger idx, BOOL *stop) {
            multiplier += obj.multiplier;
            obj.delegate = weakSelf;
            [actions addObject:[CCActionSequence actions:
                                [CCActionCallBlock actionWithBlock:^{ [obj show]; }],
                                [CCActionDelay actionWithDuration:1.0],
                                [CCActionCallBlock actionWithBlock:^{ [obj hide]; }],
                                nil]];
            
            if ([_delegate respondsToSelector:@selector(lineWin:)])
                [_delegate lineWin:obj];
        }];
        if ([winLines count] == 1)
            [actions addObject: [CCActionDelay actionWithDuration:0.5]];
        
        CCAction *action = [CCActionRepeatForever actionWithAction:[CCActionSequence actionWithArray:actions]];
        action.tag = ETWinActionTag;
        [self runAction: action];
        
        if ([_delegate respondsToSelector:@selector(winWithMultiplier:)])
            [_delegate winWithMultiplier:multiplier];
    }
    else
    {
        if ([_delegate respondsToSelector:@selector(lineLost)])
            [_delegate lineLost];
    }
    
    if ([_delegate respondsToSelector:@selector(spinEnded:bonusGame:)])
        [_delegate spinEnded:[winLines count] bonusGame:bonusGame];
}

#pragma mark ETWinLine Delegate

- (void)setLine:(NSInteger)line visible:(BOOL)visible
{
    [_lines setLine:line visible:visible];
}



@end
