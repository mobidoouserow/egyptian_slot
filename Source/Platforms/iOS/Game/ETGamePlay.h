//
//  ETGamePlay.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@interface ETGamePlay : CCNode

@property (nonatomic, assign)NSInteger line;

- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate;

@property (nonatomic, weak)id <ETEgyptianTreasuresDelegate> delegate;

- (void)spin;

@end
