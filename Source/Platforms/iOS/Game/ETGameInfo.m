//
//  ETGameInfo.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/12/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameInfo.h"
#import "ETButton.h"
#import "AppDelegate.h"

@interface ETGameInfo () <CCScrollViewDelegate>
@property (nonatomic, strong)CCScrollView *scrollView;
@property (nonatomic, strong)CCLabelTTF *pageLabel;
@property (nonatomic, assign)NSInteger page;
@end

@implementation ETGameInfo

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _page = 0;
    _contentSize = [CCDirector sharedDirector].viewSize;
    self.userInteractionEnabled = YES;
    
    [self addChild:[self nodeColor]];
    [self addChild:[self background]];
    [self addChild:[self closeButton]];
    
    self.cascadeOpacityEnabled = YES;
    self.opacity = 0.0;
    [self runAction:[CCActionFadeIn actionWithDuration:0.3]];
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (CCNodeColor *)nodeColor
{
    return [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
}

- (CCSprite *)background
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"info_back.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    
    [sprite addChild:[self backgroundEdge]];
    
    _scrollView = [self scrollView];
    [sprite addChild:_scrollView];
    [sprite addChild:[self pageLayout]];
    
    sprite.scale = 0.0f;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:0.4 scale:0.9]]
                                                     rate:3.0]];
    
    return sprite;
}

- (CCSprite *)backgroundEdge
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"info_edge.png"];
    sprite.anchorPoint = CGPointZero;
    return sprite;
}

- (CCScrollView *)scrollView
{
    CCScrollView *scroll = [CCScrollView scrollViewWithContentNode:[self contentNode]];
    scroll.pagingEnabled = YES;
    scroll.verticalScrollEnabled = NO;
    scroll.delegate = self;
    scroll.contentSizeType = CCSizeTypeNormalized;
    scroll.contentSize = CGSizeMake(1,1);
    
    return scroll;
}

- (CCNode *)contentNode
{
    CCNode *node = [CCNode node];
    NSInteger count = 3;
    for (NSInteger i = 0; i < count; i++)
    {
        [node addChild:[self contentSpriteForIndex:i count:count]];
    }
    
    node.contentSizeType = CCSizeTypeNormalized;
    node.contentSize = CGSizeMake(count, 1.0);
    
    return node;
}

- (CCSprite *)contentSpriteForIndex:(NSInteger)index count:(NSInteger)count
{
    CGFloat page = 1.0f / (float)count;
    CCSprite *sprite = [CCSprite spriteWithImageNamed:[NSString stringWithFormat:@"info_content_%li.png", (long)index+1]];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(page * 0.5 + page * index, 0.54);
    return sprite;
}

- (CCLayoutBox *)pageLayout
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self createButtonWithImageName:@"info_left_arrow.png" selector:@selector(onLeft:)]];
    [layout addChild:[self pagePanel]];
    [layout addChild:[self createButtonWithImageName:@"info_right_arrow.png" selector:@selector(onRight:)]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.09);
    layout.spacing = ETUniversalValue(10);

    return layout;
}

- (CCSprite *)pagePanel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"info_page_panel.png"];
    _pageLabel = [self createPagePanelLabel];
    [sprite addChild:_pageLabel];
    return sprite;
}

- (CCLabelTTF *)createPagePanelLabel
{
    NSString *string = [NSString stringWithFormat:@"%li-3", (long)_page + 1];
    CCLabelTTF *label = [CCLabelTTF labelWithString: string fontName:ETFontName fontSize:ETUniversalValue(20)];
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.5, 0.55);
    
    return label;
}

- (ETButton *)closeButton
{
    ETButton *button = [self createButtonWithImageName:@"close_button.png" selector:@selector(onClose:)];
    button.position = ccpSub(ccpFromSize(_contentSize), ccpFromSize(button.contentSize));
    return button;
}

- (ETButton *)createButtonWithImageName:(NSString *)imageName selector:(SEL)selector
{
    CCSpriteFrame *frame = [CCSpriteFrame frameWithImageNamed:imageName];
    ETButton *button = [ETButton buttonWithTitle:@"" spriteFrame:frame highlightedSpriteFrame:frame disabledSpriteFrame:frame];
    button.zoomWhenHighlighted = YES;
    
    [button setTarget:self selector:selector];
    return button;
}

- (void)onRight:(id)sender
{
    [[AppController sharedAppController] click];
    self.page = _scrollView.horizontalPage + 1;
}

- (void)onLeft:(id)sender
{
    [[AppController sharedAppController] click];
    self.page = _scrollView.horizontalPage - 1;
}

- (void)onClose:(id)sender
{
    [[AppController sharedAppController] click];
    [self runAction:[CCActionSequence actions:
                     [CCActionFadeOut actionWithDuration:0.3],
                     [CCActionRemove action], nil]];
}

- (void)setPage:(NSInteger)page
{
    page = MAX(MIN(page, 2), 0);
    if (_page == page)
        return;
    _page = page;
    _pageLabel.string = [NSString stringWithFormat:@"%li-3",(long)page + 1];
    [_scrollView setHorizontalPage:(int)page animated:YES];
}

#pragma mark <CCScrollViewDelegate>
- (void)scrollViewDidEndDecelerating:(CCScrollView *)scrollView
{
    _page = scrollView.horizontalPage;
    _pageLabel.string = [NSString stringWithFormat:@"%li-3",(long)_page + 1];
}

@end
