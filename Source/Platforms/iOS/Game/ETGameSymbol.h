//
//  ETGameSymbol.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCSprite.h"

@protocol ETGameSymbollDelegate <NSObject>

@end

@interface ETGameSymbol : CCSprite

@property (nonatomic, assign)ETSymbolType type;

@property (nonatomic, weak)id <ETGameSymbollDelegate> delegate;

@property (nonatomic, assign)ETTable cell;

@property (nonatomic, assign)CGSize tileSize;

@property (nonatomic, assign)BOOL replaced;

@property (nonatomic, assign)BOOL switchSymbolFrame;

@property (nonatomic, assign)NSInteger symbolFrameContour;

@property (nonatomic, assign)BOOL animate;

+ (instancetype)randomSymbolOnCell:(ETTable)cell delegate:(id <ETGameSymbollDelegate>)delegate;

+ (instancetype)symbolWithType:(ETSymbolType)type cell:(ETTable)cell delegate:(id <ETGameSymbollDelegate>)delegate;

- (instancetype)initWithSymbolType:(ETSymbolType)type cell:(ETTable)cell delegate:(id <ETGameSymbollDelegate>)delegate;

- (NSString *)symbolNameForType:(ETSymbolType)type;

- (CGPoint)calculatePositionForCell:(ETTable)cell;

- (CGPoint)calculateHighestPosition;

- (void)changeType;

- (void)hideSymbolFrame;

- (void)updateCellWithOffset:(CGPoint)offset;

@property (nonatomic, strong)CCLabelTTF *label;



@end
