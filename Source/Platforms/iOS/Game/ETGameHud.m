//
//  ETGameHud.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameHud.h"
#import "ETButton.h"
#import "ETGameSettings.h"
#import "ETGameScene.h"
#import "ETGameBackground.h"
#import "GameCenterManager.h"
#import "AppDelegate.h"

#define kAutoSpinName @"AutoSpinButtonName"
#define kInfoButtonName @"InfoButtonName"
#define kSpinButtonName @"SpinButtonName"

static float roundUp(CGFloat value)
{
    return roundf (value * 100) / 100.0;
}

@interface ETGameHud ()
@property (nonatomic ,strong)CCLabelTTF *creditsBalanceLabel;
@property (nonatomic ,strong)CCLabelTTF *linesLabel;
@property (nonatomic ,strong)CCLabelTTF *lineBetLabel;
@property (nonatomic ,strong)CCLabelTTF *totalBetLabel;
@property (nonatomic ,strong)CCLabelTTF *winLabel;
@property (nonatomic ,strong)CCLayoutBox *basePanel;
@property (nonatomic ,strong)ETButton *autoStartButton;
@property (nonatomic ,assign)NSInteger lineBet;
@property (nonatomic ,strong)NSArray *lineBetArray;

@end

@implementation ETGameHud

- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate
{
    self = [super init];
    if (!self) return(nil);
    
    _contentSize = [CCDirector sharedDirector].viewSize;
    _delegate = delegate;
    ETGameSettings *settings = [ETGameSettings defaultSettings];
    
    _lineBetArray   = @[ @125, @250, @375, @500, @625, @875, @1100, @1400, @1900, @2500 ];
    _lineBet        = MIN(_lineBetArray.count-1, settings.lineBet);
    _betLine        = [_lineBetArray[_lineBet] floatValue];
    _creditsBalance = settings.credits;
    _lines          = settings.lines;
    _totalBet       = (float)_lines * _betLine;
    _enablePanel    = YES;
    
    [self addChild: [self basePanelLayout]];
    [self addChild: [self headerLayout]];
    
#if K_CHANGE_SYMBOL_PERCENT
    [self addChild:[self symbolPercentButton]];
#endif
    
    return self;
}

- (CCLayoutBox *)basePanelLayout
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild: [self infoAndCredits]];
    [layout addChild: [self spinsAndAutoStart]];
    [layout addChild: [self linesAndLines]];
    [layout addChild: [self lineBetAndBetPerLine]];
    [layout addChild: [self totalBetAndBetMax]];
    [layout addChild: [self winAndSpin]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    
    if ([AppController sharedAppController].device == ETDeviceiPad)
        layout.position = ccp(layout.contentSize.width * 0.5, layout.contentSize.height * 0.5 + _contentSize.width * 0.05);
    else
        layout.position = ccp(layout.contentSize.width * 0.552, layout.contentSize.height * 0.5);
    
    _basePanel = layout;
    
    return layout;
}

- (CCLayoutBox *)infoAndCredits
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    [layout addChild:[self creditsBalancePanel]];
    [layout addChild:[self getCoinsButton]];
    
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)spinsAndAutoStart
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    
    _autoStartButton = [self autoStartBT];
    
    [layout addChild:_autoStartButton];
    [layout addChild:[self infoButton]];
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)linesAndLines
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    [layout addChild: [self linesButtons]];
    [layout addChild: [self linesPanel]];
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)lineBetAndBetPerLine
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    [layout addChild: [self betPerLineButton]];
    [layout addChild: [self lineBetPanel]];
    
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)totalBetAndBetMax
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    [layout addChild: [self gamebleButton]];
    [layout addChild: [self totalBetPanel]];
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)winAndSpin
{
    CCLayoutBox *layout = [[CCLayoutBox alloc ]init];
    [layout addChild: [self spinButton]];
    [layout addChild: [self winPanel]];
    layout.direction = CCLayoutBoxDirectionVertical;
    
    return layout;
}

- (CCLayoutBox *)headerLayout
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
#if K_ENABLE_GAME_CENTER
    [layout addChild:[self createButtonWithImageNamed:@"game_center.png" selector:@selector(onGameCenter:)]];
#else
    [layout addChild:[self createButtonWithImageNamed:@"rate_button_.png" selector:@selector(onRate:)]];
#endif

    [layout addChild:[self gameTitle]];
    [layout addChild:[self soundButton]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    ETGameBackground *bg = [(ETGameScene *)_delegate gameBackground];
    layout.position = ccp(bg.symbolsPosition.x,
                          bg.symbolsPosition.y + bg.symbolPanel.contentSize.height * 0.5 + layout.contentSize.height * 0.5);

    return layout;
}

- (ETButton *)symbolPercentButton
{
    ETButton *button = [self createButtonWithImageNamed:@"rate_button_.png" selector:@selector(onPercent:)];
    button.position = ccpSub(ccpFromSize(_contentSize), ccpMult(ccpFromSize(button.contentSize), 0.5));
    return button;
}

- (ETButton *)soundButton
{
    ETButton *button = [self createButtonWithImageNamed:@"Sound_Button.png" selector:@selector(onSound:)];

    CCSpriteFrame *disable = [button backgroundSpriteFrameForState:CCControlStateDisabled];
    [button setBackgroundSpriteFrame:disable forState:CCControlStateSelected];
    
    button.togglesSelectedState = YES;
    button.selected = ![ETGameSettings defaultSettings].sound;
    
    return button;
}

- (CCSprite *)gameTitle
{
    CGPoint point = [AppController sharedAppController].device == ETDeviceiPad ? ccp(0.5, 0.5) : ccp(0.5, 0.1);
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"Title_Frame.png"];
    
    [sprite addChild:[self spriteWithImageName:@"Title_Logo.png" position:point]];
    [sprite addChild:[self spriteWithImageName:@"Egyptian_Treasures_Title.png" position:ccp(0.5, 0.5)]];
    
    return sprite;
}

- (CCSprite *)spriteWithImageName:(NSString *)imageName position:(CGPoint)position
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:imageName];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = position;
    return sprite;
}

- (ETButton *)infoButton
{
    ETButton *button = [self createButtonWithImageNamed:@"info.png" selector:@selector(onInfoButton:)];
    button.name = kInfoButtonName;
    return button;
}

- (CCSprite *)creditsBalancePanel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"credits balance.png"];

    float fontSize = ETUniversalValue(12);
    CGSize dimensions = CGSizeMake(sprite.contentSize.width * 0.7, fontSize);
    
    _creditsBalanceLabel = [self creditBalanceLabelWithDimension:dimensions fontSize:fontSize];
    [sprite addChild: _creditsBalanceLabel];

    return sprite;
}

- (CCLabelTTF *)creditBalanceLabelWithDimension:(CGSize)dimension fontSize:(CGFloat)fontSize
{
    NSString *string = [NSString stringWithFormat:@"%.2f", _creditsBalance];
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:fontSize dimensions:dimension];

    label.adjustsFontSizeToFit = YES;
    label.horizontalAlignment = CCTextAlignmentRight;
    label.verticalAlignment = CCVerticalTextAlignmentCenter;
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.58, 0.41);

    return label;
}

- (ETButton *)autoStartBT
{
    ETButton *button = [self createButtonWithImageNamed:@"Auto start.png" selector:@selector(onAutoStart:)];
    button.togglesSelectedState = YES;
    button.name = kAutoSpinName;
    
    return button;
}

- (ETButton *)linesButtons
{
    return [self createButtonWithImageNamed:@"Lines.png" selector:@selector(onLines:)];
}

- (CCSprite *)linesPanel
{
    NSString *string = [NSString stringWithFormat:@"%lu", (unsigned long)_lines];
    CCSprite *sprite = [self createPanel:@"lines.png" labelString:string fontSize:ETUniversalValue(9) position:ccp(0.5, 0.41)];
    _linesLabel = sprite.children[0];
    
    return sprite;
}

- (ETButton *)betPerLineButton
{
    return [self createButtonWithImageNamed:@"Bet per line.png" selector:@selector(onBetPerLine:)];
}

- (CCSprite *)lineBetPanel
{
    NSString *string = [NSString stringWithFormat:@"%.2f",_betLine];
    CCSprite *sprite = [self createPanel:@"line bet.png" labelString:string fontSize:ETUniversalValue(9) position:ccp(0.5, 0.41)];
    _lineBetLabel = sprite.children[0];
    
    return sprite;
}

- (ETButton *)gamebleButton
{
    return [self createButtonWithImageNamed:@"bet_max.png" selector:@selector(onBetMax:)];
}

- (CCSprite *)totalBetPanel
{
    NSString *string = [NSString stringWithFormat:@"%.2f", _totalBet];
    CCSprite *sprite = [self createPanel:@"total_bet.png" labelString:string fontSize:ETUniversalValue(9) position:ccp(0.5, 0.41)];
    _totalBetLabel = sprite.children[0];
    
    return sprite;
}

- (ETButton *)getCoinsButton
{
    return [self createButtonWithImageNamed:@"get_coins.png" selector:@selector(onGetCoins:)];
}

- (ETButton *)spinButton
{
    ETButton *button = [self createButtonWithImageNamed:@"Spin.png" selector:@selector(onSpin)];
    button.name = kSpinButtonName;
    return button;
}

- (CCSprite *)winPanel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"win panel.png"];
    _winLabel = [self winPanelLabel:sprite.contentSize];
    [sprite addChild: _winLabel];
    
    return sprite;
}

- (CCLabelTTF *)winPanelLabel:(CGSize)size
{
    NSString *string = [NSString stringWithFormat:@"%.2f", _win];
    float fontSize = ETUniversalValue(12);
    CGSize dimensions = CGSizeMake(size.width * 0.62, fontSize);
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:fontSize dimensions:dimensions];
    label.adjustsFontSizeToFit = YES;
    label.horizontalAlignment = CCTextAlignmentRight;
    label.verticalAlignment = CCVerticalTextAlignmentCenter;
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.62, 0.54);

    return label;
}

- (void)linesLost
{
    float creditBalance = roundf (_creditsBalance * 100) / 100.0;
    if (creditBalance < [_lineBetArray[0] floatValue])
    {
        [_delegate shop];
    }
}

- (void)creditPlusEqual:(NSInteger)coins
{
    self.creditsBalance += coins;
    
    float fontSize = ETUniversalValue(12);
    NSString *string = [NSString stringWithFormat:@"%.2f", _creditsBalance];
    CCNode *sprite = _creditsBalanceLabel.parent;
    CGSize dimensions = CGSizeMake(sprite.contentSize.width * 0.8, fontSize);
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:fontSize dimensions:dimensions];
    label.adjustsFontSizeToFit = YES;
    label.horizontalAlignment = CCTextAlignmentCenter;
    label.verticalAlignment = CCVerticalTextAlignmentCenter;
    label.position = [_basePanel convertToWorldSpace:ccp(sprite.contentSize.width * 0.5, sprite.contentSize.height * 0.42 )];
    [self addChild: label];
    
    [label runAction: [CCActionSequence actions:
                       [CCActionEaseInOut actionWithAction:
                        [CCActionSpawn actions:
                         [CCActionScaleTo actionWithDuration:0.5 scale:1.8],
                         [CCActionFadeOut actionWithDuration:0.5] , nil] rate:0.8],
                       [CCActionRemove action], nil]];
}

#pragma mark Helper

- (CCSprite *)createPanel:(NSString *)imageName labelString:(NSString *)labelString fontSize:(CGFloat)fontSize position:(CGPoint)position
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:imageName];
    CCLabelTTF *label = [CCLabelTTF labelWithString:labelString fontName:ETFontName fontSize:fontSize];
    label.positionType = CCPositionTypeNormalized;
    label.position = position;
    [sprite addChild: label];
    
    return  sprite;
}

- (ETButton *)createButtonWithImageNamed:(NSString *)imageName selector:(SEL)selector
{
    ETButton *button = [ETButton buttonWithTitle:@""
                                     spriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:1]]
                          highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:2]]
                             disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:4]]];
    [button setBackgroundSpriteFrame:[CCSpriteFrame frameWithImageNamed:[self buttonImageName:imageName value:3]] forState:CCControlStateSelected];
    [button setTarget:self selector:selector];
    
    return button;
}

- (NSString *)buttonImageName:(NSString *)imageName value:(NSInteger)value
{
    return [NSString stringWithFormat:@"%@%li.%@",[imageName stringByDeletingPathExtension],(long)value, [imageName pathExtension]];
}

#pragma mark Setters

- (void)setCreditsBalance:(CGFloat)creditsBalance
{
    creditsBalance = MAX(creditsBalance, 0);
    if (_creditsBalance == creditsBalance)
        return;
    
    _creditsBalance = creditsBalance;
    [ETGameSettings defaultSettings].credits = creditsBalance;
    
    if (_creditsBalanceLabel == nil)
        return;
    
    _creditsBalanceLabel.string = [NSString stringWithFormat:@"%.2f", _creditsBalance];
}

- (void)setLines:(NSInteger)lines
{
    lines = MAX(lines, 1);
    if (lines > [AppController sharedAppController].winLines.count)
        lines = 1;
    
    if (_lines == lines)
        return;
    
    _lines = lines;
    
    if (_linesLabel == nil)
        return;
    
    [self.delegate line:lines];
    _linesLabel.string = [NSString stringWithFormat:@"%lu", (unsigned long)lines];
    
    self.totalBet = _lines * _betLine;
}

- (void)setLineBet:(NSInteger)lineBet
{
    lineBet = MAX(lineBet, 0);
    if (lineBet >= _lineBetArray.count)
        lineBet = 0;
    
    if (_lineBet == lineBet)
        return;
    
    _lineBet = lineBet;
    
    if (_lineBetLabel == nil)
        return;
    _betLine = [_lineBetArray[_lineBet] floatValue];
    _lineBetLabel.string = [NSString stringWithFormat:@"%.2f", _betLine];
    
    self.totalBet = _lines * _betLine;
}

- (void)setTotalBet:(float)totalBet
{
    totalBet = MAX(totalBet, 0);
    if (_totalBet == totalBet)
        return;
    
    _totalBet = totalBet;
    
    if (_totalBetLabel == nil)
        return;
    
    _totalBetLabel.string = [NSString stringWithFormat:@"%.2f", totalBet];
}

- (void)setWin:(CGFloat)win
{
    _win = win;
    if (_winLabel == nil)
        return;
    
    _winLabel.string = [NSString stringWithFormat:@"%.2f", _win];
    self.creditsBalance += _win;
}


- (void)setEnablePanel:(BOOL)enablePanel
{
    if (_enablePanel == enablePanel)
        return;
    _enablePanel = enablePanel;
    
    if (_basePanel == nil)
        return;
    [_basePanel.children enumerateObjectsUsingBlock:^(CCNode *obj, NSUInteger idx1, BOOL *stop1) {
        [obj.children enumerateObjectsUsingBlock:^(ETButton *node, NSUInteger idx2, BOOL *stop2) {
            if ([node isKindOfClass:[ETButton class]])
            {
                if (enablePanel == NO)
                {
                    if ((_autoStart == YES && [node.name isEqualToString:kAutoSpinName]) ||
                        [node.name isEqualToString:kInfoButtonName] ||
                        [node.name isEqualToString:kSpinButtonName])
                        node.enabled = YES;
                    else
                        node.enabled = enablePanel;
                }
                else
                    node.enabled = enablePanel;
                
            }
            else
            {
                [node.children enumerateObjectsUsingBlock:^(ETButton *node2, NSUInteger idx3, BOOL *stop3) {
                    if ([node2 isKindOfClass:[ETButton class]])
                        node2.enabled = enablePanel;
                }];
            }
        }];
    }];
}

- (void)betMaxLine:(NSInteger *)line lineBet:(NSInteger *)lineBet
{
    float credit = roundUp(_creditsBalance);
    NSInteger lineCount = [AppController sharedAppController].winLines.count;
    float max = ((float)lineCount * [[_lineBetArray lastObject] floatValue]);
    
    for (NSInteger a = 1; a <= lineCount; a++)
    {
        for (NSInteger b = 0; b < _lineBetArray.count; b++)
        {
            float betValue = [_lineBetArray[b] floatValue];
            float value = (float)a * betValue;
            
            float diff = credit - value;
            if (diff >= 0.0 && diff < max && value <= credit)
            {
                max = diff;
                *line = a;
                *lineBet = b;
            }
        }
    }
}

- (void)setAutoStart:(BOOL)autoStart
{
    if (_autoStart == autoStart)
        return;
    _autoStart = autoStart;
    _autoStartButton.selected = _autoStart;
}

#pragma mark Selectors

- (void)onGetCoins:(id)sender
{
    [[AppController sharedAppController] click];
    [_delegate shop];
}

- (void)onInfoButton:(id)sender
{
    [[AppController sharedAppController] click];
    [_delegate info];
}

- (void)onAutoStart:(ETButton *)sender
{
    [[AppController sharedAppController] click];
    _autoStart = sender.selected;
    if (_autoStart)
        [self onSpin];
}

- (void)onLines:(id)sender
{
    [[AppController sharedAppController] click];
    self.lines += 1;
}

- (void)onBetPerLine:(id)sender
{
    [[AppController sharedAppController] click];
    self.lineBet += 1;
}

- (void)onBetMax:(id)sender
{
    [[AppController sharedAppController] click];
    
    float credit  = roundf (_creditsBalance * 100) / 100.0;
    float maxLineBet = [[_lineBetArray lastObject] floatValue];
    NSInteger lineCount = [AppController sharedAppController].winLines.count;
    
    if (credit < (CGFloat)lineCount * maxLineBet)
    {
        NSInteger line = _lines;
        NSInteger lineBet = _lineBet;
        
        [self betMaxLine:&line lineBet:&lineBet];
        
        self.lines      = line;
        self.lineBet    = lineBet;
    }
    else
    {
        self.lines      = lineCount;
        self.lineBet    = _lineBetArray.count - 1;
    }
    [self onSpin];
}

- (void)onSpin
{
    if (_spinStarted)
        return;
    _spinStarted = YES;
    
    [ETGameSettings defaultSettings].playTime += 1;
    
    if ([self rateApp])
    {
        [_delegate rate];
        _spinStarted = NO;
    }
    else
    {
        float creditBalance = roundUp(_creditsBalance);
        float totalBet      = roundUp(_totalBet);
        if (totalBet > creditBalance || creditBalance < [_lineBetArray[0] floatValue])
        {
            self.autoStart = NO;
            self.enablePanel = YES;
            _spinStarted = NO;
            [_delegate shop];
        }
        else
        {
            self.creditsBalance -= _totalBet;

            if (![_winLabel.string isEqualToString:@"0.00"])
                _winLabel.string = @"0.00";
            
            [self.delegate spin];
        }
    }
}

- (BOOL)rateApp
{
    ETGameSettings *setting = [ETGameSettings defaultSettings];
    return !setting.rated && setting.playTime % K_RATE_APP_AFTER_SPIN_COUNT == 0;
}

- (void)onGameCenter:(id)sender
{
    [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:[CCDirector sharedDirector]
                                                           withLeaderboard:ETGCHighScoreLeaderboard];
}

- (void)onRate:(id)sender
{
    [[AppController sharedAppController] click];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ETAppLink]];
}

- (void)onSound:(ETButton *)sender
{
    [ETGameSettings defaultSettings].sound = !sender.selected;
    [[AppController sharedAppController] click];
    
    if ([ETGameSettings defaultSettings].sound)
        [[AppController sharedAppController] playBackgroundSound];
    else
        [[OALSimpleAudio sharedInstance] stopBg];
}

- (void)onPercent:(ETButton *)sender
{
    [_delegate percent];
}

@end
