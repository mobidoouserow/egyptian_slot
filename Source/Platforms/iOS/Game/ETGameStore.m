//
//  ETGameStore.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/13/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameStore.h"
#import "ETButton.h"
#import "ETStoreManager.h"
#import "AppDelegate.h"

@implementation ETGameStore

- (instancetype)init
{
    self = [super init];
    if (!self) return nil;
    
    _contentSize = [CCDirector sharedDirector].viewSize;

    [self addChild:[self nodeColor]];
    [self addChild:[self background]];
    [self addChild:[self closeButton]];
    
    self.userInteractionEnabled = YES;
    self.cascadeOpacityEnabled = YES;
    self.opacity = 0;

    [self runAction:[CCActionFadeIn actionWithDuration:0.2]];
    
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (CCNodeColor *)nodeColor
{
    return [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
}

- (CCSprite *)background
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"info_back.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    
    [sprite addChild:[self backgroundContent]];
    [sprite addChild:[self backgroundEdge]];
    
    sprite.scale = 0.0f;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:0.4 scale:0.8]]
                                                     rate:3.0]];
    return sprite;
}

- (CCLabelTTF *)title
{
    CCLabelTTF *label = [self labelWithString:@"Market" fontSize:ETUniversalValue(24)];
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.5, 0.89);
    label.color = [CCColor colorWithRed:0.639 green:0.271 blue:0.271 alpha:1];
    label.shadowColor = [CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    label.shadowOffset = ccp(0.0, -2.0);
    return label;
}

- (CCSprite *)backgroundEdge
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"info_edge.png"];
    sprite.anchorPoint = CGPointZero;
    return sprite;
}

- (CCLayoutBox *)backgroundContent
{
    __block CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
//    [layout addChild:[self getFreeCoinsButton]];
    
    NSArray *array = @[ETIAP_2p5MCoins,
                       ETIAP_1MCoins,
                       ETIAP_300KCoins,
                       ETIAP_120KCoins,
                       ETIAP_55kCoins,
                       ETIAP_20kCoins];
    
    __weak typeof(self)weakSelf = self;
    [array enumerateObjectsUsingBlock:^(NSString *_Nonnull identifier, NSUInteger idx, BOOL * _Nonnull stop) {
        [layout addChild:[weakSelf frameWithIdentifier:identifier]];
    }];
    
    [layout addChild:[self title]];
    
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.52);
    layout.spacing = ETUniversalValue(6);
    
    
    return layout;
}

- (CCSprite *)frameWithIdentifier:(NSString *)identifier
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"market_frame.png"];
    [sprite addChild:[self coinSprite]];
    [sprite addChild:[self coinLabel:identifier]];
    [sprite addChild:[self buyButtonForIdentifier:identifier]];
    return sprite;
}

- (CCSprite *)coinSprite
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"market_coin.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.15, 0.5);
    return sprite;
}

- (CCLabelTTF *)coinLabel:(NSString *)identifier
{
    CCLabelTTF *label = [self labelWithString:[self coinsForIdentifier:identifier] fontSize:ETUniversalValue(16)];
    label.anchorPoint = ccp(0.0, 0.5);
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.2, 0.55);
    return label;
}

- (CCLabelTTF *)labelWithString:(NSString *)string fontSize:(CGFloat)fontSize
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:fontSize];
    label.shadowColor = [CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    label.shadowOffset = ccp(0.0, -2.0);
    return label;
}

- (ETButton *)buyButtonForIdentifier:(NSString *)identifier
{
    NSString *title = [self priceForIdentifier:identifier];
    ETButton *button = [self buttonWithNormal:@"buy_button1.png" select:@"buy_button3.png" selector:@selector(onBuy:) title:title];
    button.positionType = CCPositionTypeNormalized;
    button.position = ccp(0.8, 0.5);
    button.name = identifier;
    return button;
}

- (ETButton *)getFreeCoinsButton
{
    return [self buttonWithNormal:@"get_free_coins1.png" select:@"get_free_coins3.png" selector:@selector(onGetFreeCoins:) title:@""];
}

- (ETButton *)closeButton
{
    ETButton *button = [self buttonWithNormal:@"close_button.png" select:@"close_button.png" selector:@selector(onClose:) title:@""];
    button.position = ccpSub(ccpFromSize(_contentSize), ccpFromSize(button.contentSize));
    return button;
}

- (ETButton *)buttonWithNormal:(NSString *)normal select:(NSString *)select selector:(SEL)selector title:(NSString *)title
{
    ETButton *button = [ETButton buttonWithTitle:title
                                     spriteFrame:[CCSpriteFrame frameWithImageNamed:normal]
                          highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:select]
                             disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:normal]];
    button.zoomWhenHighlighted = YES;
    button.label.fontName = ETFontName;
    button.label.anchorPoint = ccp(0.5, 0.35);
    button.label.fontSize = ETUniversalValue(12);
    button.label.shadowColor = [CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7];
    button.label.shadowOffset = ccp(0.0, -2.0);
    
    [button setTarget:self selector:selector];
    return button;
}

- (NSString *)priceForIdentifier:(NSString *)identifier
{
    return [[ETStoreManager sharedStoreManager] localPriceForProduct:identifier];
}

- (NSString *)coinsForIdentifier:(NSString *)identifier
{
    NSString *string = nil;
    if ([identifier isEqualToString:ETIAP_20kCoins])
        string = [self numberToCurrencyStyle:@(20000)];
    else if ([identifier isEqualToString:ETIAP_55kCoins])
        string = [self numberToCurrencyStyle:@(55000)];
    else if ([identifier isEqualToString:ETIAP_120KCoins])
        string = [self numberToCurrencyStyle:@(120000)];
    else if ([identifier isEqualToString:ETIAP_300KCoins])
        string = [self numberToCurrencyStyle:@(300000)];
    else if ([identifier isEqualToString:ETIAP_1MCoins])
        string = [self numberToCurrencyStyle:@(1000000)];
    else if ([identifier isEqualToString:ETIAP_2p5MCoins])
        string = [self numberToCurrencyStyle:@(2500000)];

    NSAssert1(string != nil, @"Could not find number for identifer: %@", identifier);
    return string;
}

- (NSString *)numberToCurrencyStyle:(NSNumber *)number
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle; //NSNumberFormatterCurrencyStyle;
    numberFormatter.currencySymbol = @"";
    
    return [numberFormatter stringFromNumber:number];
}

- (void)onBuy:(ETButton *)sender
{
    [[AppController sharedAppController] click];
    [[ETStoreManager sharedStoreManager] purchase: sender.name];
}

- (void)onClose:(id)sender
{
    [[AppController sharedAppController] click];
    [self runAction:[CCActionSequence actionOne:[CCActionFadeOut actionWithDuration:0.2] two:[CCActionRemove action]]];
}

- (void)onGetFreeCoins:(id)sender
{
    [[AppController sharedAppController] click];
    [[AppController sharedAppController] showVideoAd];
}

@end
