//
//  ETGameScene.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCScene.h"

@class ETGamePlay;

@class ETGameBackground;

@class ETGameHud;

@interface ETGameScene : CCScene <ETEgyptianTreasuresDelegate>

@property (nonatomic, strong)ETGamePlay *gamePlay;

@property (nonatomic, strong)ETGameHud *gameHud;

@property (nonatomic, strong)ETGameBackground *gameBackground;

@end
