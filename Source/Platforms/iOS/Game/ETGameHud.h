//
//  ETGameHud.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@interface ETGameHud : CCNode

@property (nonatomic, weak)id <ETEgyptianTreasuresDelegate>delegate;

@property (nonatomic ,assign)NSInteger lines;

@property (nonatomic ,assign)CGFloat win;

@property (nonatomic ,assign)CGFloat betLine;

@property (nonatomic ,assign)CGFloat creditsBalance;

@property (nonatomic ,assign)float totalBet;

@property (nonatomic ,assign)BOOL enablePanel;

@property (nonatomic ,assign)BOOL autoStart;

@property (nonatomic ,assign)BOOL spinStarted;

- (instancetype)initWithDelegate:(id <ETEgyptianTreasuresDelegate>)delegate;

- (void)creditPlusEqual:(NSInteger)coins;

- (void)linesLost;

- (void)onSpin;


@end
