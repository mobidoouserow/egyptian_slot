//
//  ETGameBackground.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameBackground.h"
#import "AppDelegate.h"

@interface ETGameBackground ()
@property (nonatomic, strong)CCSprite *background;
@property (nonatomic, strong)CCNode *buttomPanel;
@property (nonatomic, strong)CCNode *buttomNode;
@end

@implementation ETGameBackground

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    _contentSize = [CCDirector sharedDirector].viewSize;
    _device = [AppController sharedAppController].device;
    
    [self addChild:[self backgroundSprite]];
    
    return self;
}

- (CCSprite *)backgroundSprite
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"Back.png"];
    sprite.position = ccpMult(ccpFromSize(_contentSize), 0.5);
    
    _buttomNode = [self buttom];
    [sprite addChild: [self buttom] z:0];
    
    return sprite;
}

- (CCNode *)buttom
{
    CCNode *node = [CCNode node];
    
    _buttomPanel = [self buttomBase];
    _symbolPanel = [self symbolsBase: _buttomPanel];
    
    node.position = ccp(_contentSize.width * 0.5, 0);
    
    [node addChild: _symbolPanel z:1];
    [node addChild: _buttomPanel z:0];
    return node;
}

- (CCSprite *)buttomBase
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"buttom_panel.png"];

    if ([AppController sharedAppController].device == ETDeviceiPad)
        sprite.position = ccp(0, sprite.contentSize.height * 0.5 + _contentSize.width * 0.05);
    else
        sprite.position = ccp(0, sprite.contentSize.height * 0.5);

    return sprite;
}

- (CCSprite *)symbolsBase:(CCNode *)cs
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"symbols_frame_base.png"];
    sprite.position = ccp(0, sprite.contentSize.height * 0.5 + cs.position.y + cs.contentSize.height * 0.5);
    
    return sprite;
}

- (CGPoint)symbolsPosition
{
    if (_device == ETDeviceiPhone4 || _device == ETDeviceiPad)
        return [_buttomNode convertToWorldSpace:_symbolPanel.position];
    else
        return ccpAdd(_symbolPanel.position, _buttomNode.position);
}

- (NSInteger)randomFrom:(NSInteger)from to:(NSInteger)to
{
    return (int)from + arc4random() % (to-from+1);
}

@end
