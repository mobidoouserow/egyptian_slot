//
//  ETGameBonus.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/12/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@protocol ETGameBonusButtonDelegate <NSObject>

- (void)didChooseStoneButton:(id)sender;

@end

@interface ETGameBonus : CCNode <ETGameBonusButtonDelegate>

@property (nonatomic, assign)NSInteger scatterCount;

- (instancetype)initWithScatterCount:(NSInteger)scatterCount block:(void (^)(CGFloat coins))block;

@end

@interface ETGameBonusButton : CCNode

@property (nonatomic, weak)id<ETGameBonusButtonDelegate> delegate;

@property (nonatomic, assign)ETStoneSymbolType stoneType;

@property (nonatomic, assign)NSInteger coins;

@property (nonatomic, assign)BOOL enable;

- (id)initWithStoneType:(ETStoneSymbolType)stoneType coins:(NSInteger)coins;

- (void)disable;

@end
