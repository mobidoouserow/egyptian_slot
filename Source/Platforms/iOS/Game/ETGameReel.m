//
//  ETGameReel.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameReel.h"

@implementation ETGameReel

- (instancetype)init
{
    self = [super init];
    if (!self) return(nil);
    
    return self;
}

- (void)createSymbols
{
    
    for (NSInteger i = 0; i < ETSlotColumnCount; i++)
    {
        ETGameSymbol *symbol = [ETGameSymbol randomSymbolOnCell:ETTableMake(0, 0) delegate:self];
        symbol.position = [self calculatePosition:self.position size:symbol.contentSize idx:i];
        [self addChild:symbol];
    }
}

- (CGPoint)calculatePosition:(CGPoint)position size:(CGSize)size idx:(NSInteger)idx
{
    float spaceing =  2.0;
    float offset = size.height + spaceing;
    
    return ccp(position.x, position.y + offset * ( (float)idx - 0.5f*(ETSlotColumnCount - 1.0f) ));
}


@end
