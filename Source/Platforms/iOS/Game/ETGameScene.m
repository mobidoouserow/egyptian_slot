//
//  ETGameScene.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameScene.h"

#import "ETGamePlay.h"
#import "ETGameHud.h"
#import "ETGameBackground.h"
#import "ETGameBonus.h"
#import "ETGameRate.h"
#import "ETGameInfo.h"
#import "ETGameStore.h"
#import "ETPercentSettings.h"
#import "AppDelegate.h"
#import "ETParticle.h"

@interface ETGameScene ()

@end

@implementation ETGameScene

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    [[AppController sharedAppController] playBackgroundSound];
    _gameBackground = [[ETGameBackground alloc] init];
    [self addChild:_gameBackground];
    
    _gamePlay = [[ETGamePlay alloc] initWithDelegate:self];
    [self addChild:_gamePlay];

    _gameHud = [[ETGameHud alloc] initWithDelegate:self];
    [self addChild:_gameHud];
    
    [self start];

    return self;
}

- (void)start
{
    // game loaded and ready to spin
}

- (void)spin
{
    [_gamePlay spin];
}

- (void)line:(NSInteger)line
{
    self.gamePlay.line = line;
}

- (void)shop
{
    [self addChild:[[ETGameStore alloc] init]];
}

- (void)rate
{
    [self addChild:[[ETGameRate alloc] initWithDelegate:self]];
}

- (void)info
{
    [self addChild:[[ETGameInfo alloc] init]];
}

- (void)particle:(NSInteger)count
{
    [self addChild:[[ETParticleCoin alloc] initWithTotalParticles:count * 0.85]];
    [self addChild:[[ETParticleStar alloc] initWithTotalParticles:count * 0.15]];
}

- (void)bonus:(NSInteger)count
{
    __weak typeof(_gameHud)weakGameHud = _gameHud;
    ETGameBonus *gameBonus = [[ETGameBonus alloc] initWithScatterCount:count block:^(CGFloat coins) {
        weakGameHud.creditsBalance += coins;
        if (weakGameHud.autoStart)
            [weakGameHud onSpin];
    }];
    [self addChild:gameBonus];
}

- (void)percent
{
    [self addChild:[[ETPercentSettings alloc] init]];
}

@end
