//
//  ETGameReelsFrame.h
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/6/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "CCNode.h"

@class ETWinLine;
@protocol ETGameReelsFrameDelegate <NSObject>

@required
- (void)winWithMultiplier:(NSInteger)multiplier;
- (void)bonusGame:(NSInteger)scatterCount;
@optional

- (void)lineWin:(ETWinLine *)win;
- (void)setReelLine:(NSInteger)line;
- (void)spinEnded:(NSInteger)winLineCount bonusGame:(BOOL)bonusGame;
- (void)spinStarted;
- (void)lineLost;

@end


@class ETGameLines;

@interface ETGameReelsFrame : CCNode

@property (nonatomic, weak)id <ETGameReelsFrameDelegate> delegate;

@property (nonatomic, strong)ETGameLines *lines;

- (instancetype)initWithDelegate:(id <ETGameReelsFrameDelegate>)delegate;

- (void)spin;

- (void)clean;


@end
