//
//  ETGameBonus.m
//  EgyptianTreasures
//
//  Created by Mirkobil Mirpayziev on 3/12/16.
//  Copyright © 2016 Mirkobil Mirpayziev. All rights reserved.
//

#import "ETGameBonus.h"
#import "ETButton.h"
#import "AppDelegate.h"

static NSString *const d_stone  = @"stone";
static NSString *const d_coins    = @"coins";

@interface ETGameBonus ()
@property (nonatomic, assign)NSInteger currColumn;
@property (nonatomic, assign)NSInteger column;
@property (nonatomic, assign)NSInteger from;
@property (nonatomic, assign)NSInteger to;
@property (nonatomic, strong)CCNode *arrowNode;
@property (nonatomic, strong)CCLayoutBox *contentLayout;
@property (nonatomic, assign)CGFloat total;
@property (nonatomic, strong)CCLabelTTF *totalLabel;
@property (nonatomic, assign)NSInteger multiplier;
@property (nonatomic, strong)CCSprite *sunSprite;
@property (nonatomic, assign)ETStoneSymbolType stoneType;
@property (nonatomic,copy) void(^block)(CGFloat coins);

@end

@implementation ETGameBonus

- (instancetype)initWithScatterCount:(NSInteger)scatterCount block:(void (^)(CGFloat coins))block
{
    self = [super init];
    if (!self) return nil;
    
    _column = 0;
    _total = 0;
    _multiplier = arc4random_uniform(4) + 2;
    _scatterCount = scatterCount;
    _block = block;
    
    NSInteger value = [self multiplier:scatterCount];
    
    _from = K_BONUS_GAME_MIN * value;
    _to = K_BONUS_GAME_MAX * value;
    _contentSize = [CCDirector sharedDirector].viewSize;
    _stoneType = ETStoneSymbolT;
    

    [self addChild:[self nodeColor]];
    [self addChild:[self appearPanel]];

    self.userInteractionEnabled = YES;
    self.cascadeOpacityEnabled = YES;
    self.opacity = 0;
    
    [self runAction:[CCActionFadeIn actionWithDuration:0.2]];
    
    return self;
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event
{
    
}

- (NSInteger)multiplier:(NSInteger)scatter
{
    NSInteger value = 1;
    switch (scatter)
    {
        case 3: value = 1; break;
        case 4: value = 8; break;
        case 5: value = 30; break;
        default: break;
    }
    return value;
}

- (CCNodeColor *)nodeColor
{
    return [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]];
}


- (CCSprite *)background
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"bonus_game_back.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);

    _contentLayout = [self content];
    [sprite addChild:_contentLayout];
    
    [sprite addChild:[self totalLayout]];
    [sprite addChild:[self mulitplierLabel]];

    _sunSprite = [self sun];
    [sprite addChild:_sunSprite];
    
    _arrowNode = [CCNode node];
    [sprite addChild:_arrowNode z:1];
    
    sprite.scale = 0.0f;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:0.8 scale:1.0]]
                                                     rate:3.0]];
    
    return sprite;
}

- (CCLayoutBox *)content
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
 
    [layout addChild:[self createLayoutWithCoins:[self layoutArrayCount:4] name:@"1"]];
    [layout addChild:[self createLayoutWithCoins:[self layoutArrayCount:3] name:@"2"]];
    [layout addChild:[self createLayoutWithCoins:[self layoutArrayCount:2] name:@"3"]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.direction = CCLayoutBoxDirectionVertical;
    
    if ([AppController sharedAppController].device == ETDeviceiPad)
    {
        layout.position = ccpAdd(CGPointZero, ccp(_contentSize.width * 0.507, _contentSize.height * 0.318));
        layout.spacing = ETUniversalValue(14);
    }
    else
    {
        layout.position = ccpAdd(CGPointZero, ccp(_contentSize.width * 0.507, _contentSize.height * 0.318));
        layout.spacing = ETUniversalValue(9);
    }
    

    
    return layout;
}

- (CCSprite *)sun
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"bonus_game_sun.png"];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5083, 0.882);
    return sprite;
}

- (CCLabelTTF *)mulitplierLabel
{
    NSString *string = [NSString stringWithFormat:@"X%li", (long)_multiplier];
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:ETUniversalValue(20)];
    label.color = [CCColor colorWithRed:0.992 green:0.961 blue:0.718 alpha:1];
    label.positionType = CCPositionTypeNormalized;
    label.position = ccp(0.507, 0.88);

    return label;
}

- (CCLayoutBox *)totalLayout
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    _totalLabel = [self createLabelWithString:[NSString stringWithFormat:@"%li", (long)_total]
                                        color:[CCColor colorWithRed:0.992 green:0.961 blue:0.718 alpha:1]];
    [layout addChild:[self createLabelWithString:@"TOTAL:" color:[CCColor colorWithRed:1.000 green:0.871 blue:0.157 alpha:1]]];
    [layout addChild:_totalLabel];
    
    
    layout.direction = CCLayoutBoxDirectionHorizontal;
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.68);
    
    return layout;
}

- (CCLabelTTF *)createLabelWithString:(NSString *)string color:(CCColor *)color
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:ETUniversalValue(14)];
    label.color = color;
    return label;
}

- (CCLayoutBox *)createLayoutWithCoins:(NSArray *)array name:(NSString *)name
{
    __block CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    BOOL enable = [name integerValue] == 1;
    __weak typeof(self)weakSelf = self;
    [array enumerateObjectsUsingBlock:^(NSDictionary *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ETGameBonusButton *button = [[ETGameBonusButton alloc] initWithStoneType:[obj[d_stone] integerValue] coins:[obj[d_coins] integerValue]];
        button.enable = enable;
        button.delegate = weakSelf;
        [layout addChild:button];
    }];
    
    layout.name = name;
    
    if ([AppController sharedAppController].device == ETDeviceiPad)
    {
        layout.spacing = ETUniversalValue(18);
    }
    else
    {
        layout.spacing = ETUniversalValue(13);
    }
    
    return layout;
}

- (NSArray *)layoutArrayCount:(NSInteger)count
{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:count];
    for (NSInteger i = count; i > 0; i--)
    {
        [array addObject:[self dictionaryWithStone:_stoneType]];
        _stoneType -= 1;
    }
    
    _column = MIN(_column + 1, 3);
    array[arc4random_uniform((u_int32_t)array.count)][d_coins] = 0;
    
    return array;
}

- (NSMutableDictionary *)dictionaryWithStone:(ETStoneSymbolType)stoneType
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSInteger value = (_to - _from) / 3;
    NSInteger coin = MAX(1, _from + value * _column + arc4random_uniform((u_int32_t)value));
    
    [dictionary setValue:@(stoneType) forKey:d_stone];
    [dictionary setValue:@(coin) forKey:d_coins];
    
    return dictionary;
}

- (void)setCurrColumn:(NSInteger)currColumn
{
    if (_currColumn == currColumn)
        return;
    _currColumn = currColumn;

    CCLayoutBox *layout = nil;
    for (CCLayoutBox *l in _contentLayout.children)
    {
        if ([l.name isEqualToString:[NSString stringWithFormat:@"%li", (long)currColumn]])
        {
            layout = l;
            break;
        }
    }
    
    NSAssert1(layout != nil, @"Could not find layout with name: %li", (long)currColumn);

    for (CCNode *node in _arrowNode.children)
    {
        [node runAction:[CCActionSequence actions: [CCActionFadeTo actionWithDuration:1.0 opacity:0.0], [CCActionRemove action] , nil]];
    }

    [_arrowNode addChild:[self createArrowsForLayout:layout]];
    [self enableLayout:currColumn];
}

- (void)setTotal:(CGFloat)total
{
    _total = total;
    _totalLabel.string = [NSString stringWithFormat:@"%li", (long)total];
}

- (CCNode *)createArrowsForLayout:(CCLayoutBox *)layout
{
    CCNode *node = [CCNode node];
    node.anchorPoint = ccp(0.5, 0.5);
    node.contentSize = layout.contentSizeInPoints;
    
    [node addChild:[self arrowLeft]];
    [node addChild:[self arrowRight]];
    

    CGPoint offset = ccpMult(ccpFromSize(_contentLayout.contentSizeInPoints), 1.0/_contentLayout.children.count);
    CGPoint point = _contentLayout.position;
    switch ([layout.name integerValue]) {
        case 1: point = ccpSub(point, ccp(0.0, offset.y)); break;
        case 3: point = ccpAdd(point, ccp(0.0, offset.y)); break;
        default: break;
    }
    node.cascadeOpacityEnabled = YES;
    node.position = point;
    
    return node;
}

- (CCSprite *)arrowLeft
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"bonus_game_arrow1.png"];
    sprite.anchorPoint = ccp(1.0, 0.5);
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(-0.5,0.5);
    sprite.opacity = 0.0;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionSpawn actionOne:[CCActionMoveTo actionWithDuration:0.5 position:ccp(0.0,0.5)]
                                            two:[CCActionFadeIn actionWithDuration:0.5]]
                                                   rate:2.0]];
    return sprite;
}

- (CCSprite *)arrowRight
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"bonus_game_arrow2.png"];
    sprite.anchorPoint = ccp(0.0, 0.5);
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(1.5,0.5);
    sprite.opacity = 0.0;
    [sprite runAction:[CCActionEaseInOut actionWithAction:
                       [CCActionSpawn actionOne:[CCActionMoveTo actionWithDuration:0.5 position:ccp(1.0,0.5)]
                                            two:[CCActionFadeIn actionWithDuration:0.5]]
                                                   rate:2.0]];
    return sprite;
}

- (void)enableLayout:(NSInteger)layoutNumber
{
    __block NSString *string = [NSString stringWithFormat:@"%li", (long)layoutNumber];
    [_contentLayout.children enumerateObjectsUsingBlock:^(CCLayoutBox *_Nonnull layout, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([layout.name isEqualToString:string])
        {
            [layout.children enumerateObjectsUsingBlock:^(ETGameBonusButton *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
                button.enable = YES;
            }];
        }
    }];
}

- (void)clean
{
    _block(_total);
    self.cascadeOpacityEnabled = YES;
    [self runAction:[CCActionSequence actions:
                     [CCActionFadeTo actionWithDuration:0.3 opacity:0.0],
                     [CCActionRemove action]
                     , nil]];
}

- (void)close
{
    if ([self getChildByName:@"colorNode" recursively:NO])
        return;
    CCNodeColor *node = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    
    [node addChild:[self winPanel]];
    [self addChild:node z:1 name:@"colorNode"];
    [node runAction:[CCActionSequence actions:
                     [CCActionFadeTo actionWithDuration:0.3 opacity:0.7],
                     [CCActionDelay actionWithDuration:1.2],
                     [CCActionCallFunc actionWithTarget:self selector:@selector(clean)], nil]];
}

- (CCSprite *)appearPanel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"win back.png"];

    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    sprite.scale = 0.1;
    sprite.cascadeOpacityEnabled = YES;
    
    [sprite addChild:[self layoutAppear]];
    
    __weak typeof(self)weakSelf = self;
    [sprite runAction:[CCActionSequence actions:
                       [CCActionEaseInOut actionWithAction:
                        [CCActionEaseBackOut actionWithAction:
                         [CCActionScaleTo actionWithDuration:0.6 scale:1.0]] rate:3.0],
                       [CCActionDelay actionWithDuration:2.0],
                       [CCActionSpawn actions:
                        [CCActionScaleTo actionWithDuration:0.3 scale:0.0],
                        [CCActionFadeOut actionWithDuration:0.3],
                        [CCActionCallBlock actionWithBlock:^
                         {
                             [weakSelf addChild:[weakSelf background]];
                             weakSelf.currColumn = 1;
                         }], nil],
                       [CCActionRemove action], nil]];
    return sprite;

}

- (CCLayoutBox *)layoutAppear
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self labelWithString:@"Bonus Game"
                                     color:[CCColor colorWithRed:0.616 green:0.000 blue:0.000 alpha:1]
                                  fontSize:ETUniversalValue(30)]];
    [layout addChild:[self labelWithString:@"You Win!"
                                     color:[CCColor colorWithRed:0.427 green:0.255 blue:0.137 alpha:1]
                                  fontSize:ETUniversalValue(26)]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.5);
    layout.spacing = ETUniversalValue(10);

    return layout;
}

- (CCSprite *)winPanel
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:@"win back.png"];
    
    sprite.cascadeOpacityEnabled = YES;
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    sprite.scale = 0.1;
    sprite.opacity = 0.1;
    
    [sprite addChild:[self layoutWin]];
    
    [sprite runAction:[CCActionFadeIn actionWithDuration:0.3]];
    [sprite runAction:[CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:0.3 scale:1.0]]];
    
    return sprite;
}

- (CCLayoutBox *)layoutWin
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];
    
    [layout addChild:[self labelWithString:[NSString stringWithFormat:@"%li", (long)_total]
                                     color:[CCColor colorWithRed:0.616 green:0.000 blue:0.000 alpha:1]
                                  fontSize:ETUniversalValue(36)]];
    [layout addChild:[self labelWithString:@"You Win"
                                     color:[CCColor colorWithRed:0.427 green:0.255 blue:0.137 alpha:1]
                                  fontSize:ETUniversalValue(26)]];
    
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.5);
    layout.spacing = ETUniversalValue(10);
    
    return layout;
}

- (CCLabelTTF *)labelWithString:(NSString *)string color:(CCColor *)color fontSize:(CGFloat)fontSize
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:fontSize];
    label.color = color;
    return label;
}

- (void)didChooseStoneButton:(ETGameBonusButton *)sender
{
    self.total += sender.coins;
    [sender.parent.children enumerateObjectsUsingBlock:^(ETGameBonusButton *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj disable];
    }];
    
    if (sender.coins > 0)
    {
        if (_currColumn == 3)
        {
            __weak typeof(self)weakSelf = self;
            [_sunSprite runAction:[CCActionSequence actions:
                                   [CCActionSpawn actions:
                                    [CCActionScaleTo actionWithDuration:0.3 scale:0.0],
                                    [CCActionRotateBy actionWithDuration:0.3 angle:360], nil],
                                   [CCActionDelay actionWithDuration:1.0],
                                   [CCActionCallBlock actionWithBlock:^{
                weakSelf.total = _total * _multiplier;
                [weakSelf close];
            }], nil]];
        }
        else
        {
            self.currColumn = MIN(_currColumn + 1, _contentLayout.children.count);
        }
    }
    else
    {
        [self runAction:[CCActionSequence actionOne:[CCActionDelay actionWithDuration:1.0]
                                                two:[CCActionCallFunc actionWithTarget:self selector:@selector(close)]]];
    }
}

@end

@interface ETGameBonusButton ()
@property (nonatomic, strong)ETButton *button;
@property (nonatomic, strong)CCSprite *frame;
@end

@implementation ETGameBonusButton

- (id)initWithStoneType:(ETStoneSymbolType)stoneType coins:(NSInteger)coins
{
    if (!(self = [super init])) return nil;
    
    _coins = coins;
    _stoneType = stoneType;
    _enable = YES;
    _frame = [self spriteWithImageNamed:@"bonus_game_frames.png"];
    _frame.visible = NO;
    [self addChild:_frame];
    
    _button = [self buttonWithType:stoneType];
    [self addChild:_button];
    
    _contentSize = _button.contentSize;
    
    return self;
}

- (ETButton *)buttonWithType:(ETStoneSymbolType)type
{
    CCSpriteFrame *frame = [CCSpriteFrame frameWithImageNamed:[self imageNameForStoneType:type]];
    CCSpriteFrame *select = [self selectedSpriteFrameWithSize:frame.texture.contentSize];
    
    ETButton *button = [ETButton buttonWithTitle:@"" spriteFrame:frame highlightedSpriteFrame:frame disabledSpriteFrame:frame];
    [button setBackgroundSpriteFrame:select forState:CCControlStateSelected];
    button.togglesSelectedState = YES;
    button.positionType = CCPositionTypeNormalized;
    button.position = ccp(0.5, 0.5);
    [button setTarget:self selector:@selector(onButton:)];
    
    
    return button;
}

- (CCSpriteFrame *)selectedSpriteFrameWithSize:(CGSize)size
{
    CCNode *node = [CCNode node];
    node.contentSize = size;

    if (_coins <= 0)
        [node addChild:[self spriteWithImageNamed:@"bonus_game_skulls.png"]];
    else
        [node addChild:[self layout]];

    return [self renderNode:node];
}

- (CCLayoutBox *)layout
{
    CCLayoutBox *layout = [[CCLayoutBox alloc] init];

    [layout addChild:[CCSprite spriteWithImageNamed:@"bonus_game_coins.png"]];
    [layout addChild:[self labelWithString:[NSString stringWithFormat:@"%li", (long)_coins]]];
    
    layout.positionType = CCPositionTypeNormalized;
    layout.position = ccp(0.5, 0.5);
    layout.anchorPoint = ccp(0.5, 0.5);
    layout.direction = CCLayoutBoxDirectionVertical;
    layout.spacing = ETUniversalValue(2);

    return layout;
}

- (CCSpriteFrame *)renderNode:(CCNode *)node
{
    CCRenderTexture *render = [CCRenderTexture renderTextureWithWidth:node.contentSize.width height:node.contentSize.height];
    
    [render begin];
    
    [node visit];
    
    [render end];
    
    return [render.texture createSpriteFrame];
}

- (CCLabelTTF *)labelWithString:(NSString *)string
{
    CCLabelTTF *label = [CCLabelTTF labelWithString:string fontName:ETFontName fontSize:ETUniversalValue(16)];
    label.color = [CCColor colorWithRed:0.996 green:0.957 blue:0.718 alpha:1];
    return label;
}

- (CCSprite *)spriteWithImageNamed:(NSString *)imageName
{
    CCSprite *sprite = [CCSprite spriteWithImageNamed:imageName];
    sprite.positionType = CCPositionTypeNormalized;
    sprite.position = ccp(0.5, 0.5);
    return sprite;
}

- (NSString *)imageNameForStoneType:(ETStoneSymbolType)stoneType
{
    NSString *string = [NSString stringWithFormat:@"bonus_game_plate_%li.png",(long)stoneType - (ETStoneSymbol6 - 1)];
    return string;
}

- (void)setEnable:(BOOL)enable
{
    if (_enable == enable)
        return;
    _enable = enable;
    _button.enabled = enable;
}

- (void)disable
{
    CCSpriteFrame *frame = [_button backgroundSpriteFrameForState:CCControlStateSelected];
    [_button setBackgroundSpriteFrame:frame forState:CCControlStateDisabled];
    self.enable = NO;
}

- (void)onButton:(ETButton *)sender
{
    [[AppController sharedAppController] click];
    _frame.visible = YES;
    [_delegate didChooseStoneButton:self];
}
@end

