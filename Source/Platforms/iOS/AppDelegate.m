/*
 */

#import "cocos2d.h"

#import "UIWebViewController.h"
#import "AppDelegate.h"
#import "ETGameScene.h"
#import "ETAnimationManager.h"
#import "ETGameHud.h"
#import "GameCenterManager.h"
#import <Chartboost/Chartboost.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface AppController () <GameCenterManagerDelegate, ChartboostDelegate, GADRewardBasedVideoAdDelegate>
@property (nonatomic, assign)double totalChance;
@end

@implementation AppController

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSString *yandexMetrickakey = @"34b2cdf9-1173-4f26-9673-d4373e824b75";
    [YMMYandexMetrica activateWithApiKey:yandexMetrickakey];
    
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    
    NSString *string = [carrier isoCountryCode];
    NSArray *aprovedCarrier = @[
                                @"ru",@"az",@"am",@"by",@"ge",@"kg",@"kz",@"md",@"tj",@"tm",@"ua",@"uz",@"lt",@"lv",@"ee",
                                //        @"AL",@"AD",@"AT",@"BY",@"BE",@"BA",@"BG",@"HR",@"CY",@"CZ",@"DK",@"EE",@"FO",@"FI",@"FR",@"DE",@"GI",@"GR",@"HU",@"IS",@"IE",@"IM",@"IT",@"RS",@"LV",@"LI",@"LT",@"LU",@"MK",@"MT",@"MD",@"MC",@"ME",@"NL",@"NO",@"PL",@"PT",@"RO",@"RU",@"SM",@"RS",@"SK",@"SI",@"ES",@"SE",@"CH",@"UA",@"GB",@"VA",@"RS"
                                ];
    BOOL test = NO;
    for (NSString *code in aprovedCarrier) {
        if ( [[code lowercaseString] isEqualToString:[string lowercaseString]]) {
            test = YES;
            break;
        }
    }
//        
//    if (string != nil && ![[string lowercaseString] isEqualToString:@"us"]) {
//        test = YES;
//    }
    if(test) {
        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.window.rootViewController = [[UIWebViewController alloc] initWithNibName:@"UIWebViewController" bundle:nil];
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        return YES;
    }
#ifdef APPORTABLE
    if([cocos2dSetup[CCSetupScreenMode] isEqual:CCScreenModeFixed])
        [UIScreen mainScreen].currentMode = [UIScreenMode emulatedMode:UIScreenAspectFitEmulationMode];
    else
        [UIScreen mainScreen].currentMode = [UIScreenMode emulatedMode:UIScreenScaledAspectFitEmulationMode];
#endif
    
    [self setupCocos2dWithOptions:@{ /*CCSetupShowDebugStats: @(YES),*/
                                    CCSetupDepthFormat : @GL_DEPTH24_STENCIL8_OES, }];
    
    //    GameCenter
#if K_ENABLE_GAME_CENTER
    [[GameCenterManager sharedManager] setupManager];
    [[GameCenterManager sharedManager] setDelegate:self];
#endif
    
    //    Cache Animations
    [[ETAnimationManager sharedAnimationManager] cacheSpriteFramesFromFiles:[NSSet setWithObjects: @"symbol_animation.plist",nil]
                                                                   metadata:@"animation.plist"];
    
    //    Chartboost
    [Chartboost startWithAppId:ETChartboostAppId appSignature:ETChartboostAppSignature delegate:self];
    [Chartboost cacheRewardedVideo:CBLocationHomeScreen];
    
    //    Admob
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    [self admobRequestVideoAds];
    
    _symbolPercent = [NSMutableArray arrayWithObjects:@8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33, @8.33,  nil];
    _totalChance = [self getTotal:_symbolPercent];
    
    [[OALSimpleAudio sharedInstance] preloadBg:@"casino_noice.mp3"];
    
    // Store settings
    
    [[ETStoreManager sharedStoreManager] startWithProducts:[NSSet setWithObjects:
                                                            ETIAP_20kCoins,
                                                            ETIAP_55kCoins,
                                                            ETIAP_120KCoins,
                                                            ETIAP_300KCoins,
                                                            ETIAP_1MCoins,
                                                            ETIAP_2p5MCoins, nil] delegate:self];
    
    return YES;
}

- (CCScene*) startScene
{
    return [ETGameScene node];
}

+ (instancetype)sharedAppController
{
    return (AppController *)[[UIApplication sharedApplication] delegate];
}

- (void)click
{
    [self playEffect:@"button_click.mp3"];
}

- (void)playEffect:(NSString *)effect
{
    [self playEffect:effect loop:NO];
}

- (void)playBackgroundSound
{
    if (![ETGameSettings defaultSettings].sound)
        return;
    
    OALSimpleAudio *audio = [OALSimpleAudio sharedInstance];
    if (![audio bgPlaying])
    {
        [audio playBg:@"casino_noice.mp3" loop:YES];
        [audio setBgVolume:0.1];
    }
}

- (id<ALSoundSource>)playEffect:(NSString *)sound loop:(bool)loop
{
    return (![ETGameSettings defaultSettings].sound) ? nil : [[OALSimpleAudio sharedInstance] playEffect:sound loop:loop];
}

- (void)showVideoAd
{
    if ([[GADRewardBasedVideoAd sharedInstance] isReady])
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:[CCDirector sharedDirector]];
    else if ([Chartboost hasRewardedVideo:CBLocationHomeScreen])
        [Chartboost showRewardedVideo:CBLocationHomeScreen];
}

- (void)admobRequestVideoAds
{
    GADRequest *request = [GADRequest request];
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request withAdUnitID:ETAdMobVideoRewardId];
}

- (CGFloat)getTotal:(NSArray *)array
{
    __block CGFloat total = 0;
    [array enumerateObjectsUsingBlock:^(NSNumber *_Nonnull number, NSUInteger idx, BOOL * _Nonnull stop) {
        total += [number floatValue];
    }];
    return total;
}

- (NSArray *)winLines
{
    if (_winLines == nil)
        _winLines = [NSMutableArray arrayWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"lines.plist"]];
    
    return _winLines;
}

- (ETSymbolType)smartRandomSymbol
{
    double random = ETFloatRandom(1, _totalChance);
    ETSymbolType idx = 0;
    
    for (NSInteger i = 0; i < _symbolPercent.count; i++)
    {
        random -= [_symbolPercent[i] floatValue];
        if ( random <= 0 )
        {
            idx = i;
            break;
        }
    }
    
    NSAssert1(idx + 1 <= ETSymbol12, @"Wrong symbol type for random value: %f", random);
    return idx + 1;
}


- (ETDevice)device
{
    if (_device == ETDeviceNone)
    {
        NSInteger device = [[CCConfiguration sharedConfiguration] runningDevice];
        switch (device)
        {
            case CCDeviceiPhone5RetinaDisplay:  case CCDeviceiPhone5:   _device =  ETDeviceiPhone5; break;
            case CCDeviceiPhoneRetinaDisplay:   case CCDeviceiPhone:    _device =  ETDeviceiPhone4; break;
            case CCDeviceiPadRetinaDisplay:     case CCDeviceiPad:      _device =  ETDeviceiPad;    break;
            case CCDeviceiPhone6:     _device =  ETDeviceiPhone6;     break;
            case CCDeviceiPhone6Plus: _device =  ETDeviceiPhone6Plus; break;
            default: _device = ETDeviceiPhone5; break;
        }
    }
    return _device;
}

- (void)purchasedCoins:(CGFloat)coins
{
    ETGameSettings *settings = [ETGameSettings defaultSettings];
    
    CCScene *current = [[CCDirector sharedDirector] runningScene];
    if ([current isKindOfClass:[ETGameScene class]])
        ((ETGameScene *)current).gameHud.creditsBalance += coins;
    else
        settings.credits += coins;
}

#pragma mark <ETStoreDelegate>

- (void)transactionSuccessfulForIdentifier:(NSString *)identifier
{
    if ([identifier isEqualToString:ETIAP_20kCoins])
        [self purchasedCoins:20000];
    else if ([identifier isEqualToString:ETIAP_55kCoins])
        [self purchasedCoins:55000];
    else if ([identifier isEqualToString:ETIAP_120KCoins])
        [self purchasedCoins:120000];
    else if ([identifier isEqualToString:ETIAP_300KCoins])
        [self purchasedCoins:300000];
    else if ([identifier isEqualToString:ETIAP_1MCoins])
        [self purchasedCoins:1000000];
    else if ([identifier isEqualToString:ETIAP_2p5MCoins])
        [self purchasedCoins:2500000];
}

#pragma mark GameCenterManager Delegate Methods

- (void)gameCenterManager:(GameCenterManager *)manager authenticateUser:(UIViewController *)gameCenterLoginController
{
    [[CCDirector sharedDirector] presentViewController:gameCenterLoginController animated:YES completion:NULL];
}

#pragma mark <ChartboostDelegate>
- (void)didCompleteRewardedVideo:(CBLocation)location withReward:(int)reward
{
    NSLog(@"Chartboost reward amount %i", reward);
    [self purchasedCoins:reward];
}

- (void)didCacheRewardedVideo:(CBLocation)location
{
    NSLog(@"Chartboost Reward based video ad is closed.");
}

#pragma mark <GADRewardBasedVideoAdDelegate>
/*
 - (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd didRewardUserWithReward:(GADAdReward *)reward
 {
 NSLog(@"Admob reward amount %f", [reward.amount doubleValue]);
 [self purchasedCoins:[reward.amount doubleValue]];
 }
 */

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Admob Reward based video ad is closed.");
    [self admobRequestVideoAds];
    [self purchasedCoins:2000];
}


@end
