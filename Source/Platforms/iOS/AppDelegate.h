/*
 */

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "ETStoreManager.h"

@interface AppController : CCAppDelegate <ETStoreDelegate>

+ (instancetype)sharedAppController;

@property (nonatomic, strong)NSArray *winLines;

@property (nonatomic, assign)ETDevice device;

@property (nonatomic, strong)NSMutableArray *symbolPercent;

- (ETSymbolType)smartRandomSymbol;

- (void)click;

- (void)playEffect:(NSString *)effect;

- (id<ALSoundSource>)playEffect:(NSString *)sound loop:(bool)loop;

- (void)playBackgroundSound;

- (void)showVideoAd;

@end
